<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $data['banner'] = DB::table('banner')->limit(4)->get();

        $data['first_news'] = DB::table('news')->where('ne_cate',1)->orderBy('ne_id','DESC')->limit(1)->get();
        $data['last_news'] = DB::table('news')->where('ne_cate',1)->orderBy('ne_id','DESC')->take(4)->skip(1)->get();

        $data['first_calendar'] = DB::table('news')->where('ne_cate',2)->orderBy('ne_id','DESC')->limit(1)->get();
        $data['last_calendar'] = DB::table('news')->where('ne_cate',2)->orderBy('ne_id','DESC')->take(4)->skip(1)->get();

        $data['first_regime'] = DB::table('news')->where('ne_cate',3)->orderBy('ne_id','DESC')->limit(1)->get();
        $data['last_regime'] = DB::table('news')->where('ne_cate',3)->orderBy('ne_id','DESC')->take(4)->skip(1)->get();

        $data['first_student'] = DB::table('news')->where('ne_cate',4)->orderBy('ne_id','DESC')->limit(1)->get();
        $data['last_student'] = DB::table('news')->where('ne_cate',4)->orderBy('ne_id','DESC')->take(4)->skip(1)->get();

        return view('fontend.page.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
