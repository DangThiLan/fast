/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//http://[tên miền của bạn]/ckfinder/ckfinder.html’
	config.filebrowserBrowseUrl = 'http://fast.abc/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = 'http://fast.abc/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = 'http://fast.abc/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = 'http://fast.abc/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = 'http://fast.abc/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = 'http://fast.abc/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
