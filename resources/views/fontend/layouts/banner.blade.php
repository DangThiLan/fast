<header id="banner" class="header">
   <div class="banner-wrap">
      <div class="swiper-container">
         <div class="swiper-wrapper">
            @foreach($banner as $bn)
            <div class="swiper-slide">
               <div class="banner-img">
                  <img src="{{asset('uploads/images/banner/'.$bn->bn_image)}}">
               </div>
               <div class="container">
                  <div class="banner-txt">
                     <h2 class="title"></h2>
                     <p></p>
                  </div>
               </div>
            </div>
            @endforeach
         </div>
         <div class="swiper-pagination"></div>
      </div>
   </div>
</header>