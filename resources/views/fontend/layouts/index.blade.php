<!DOCTYPE html>
<!--[if IE 8]>
<html lang="vi"
   class="ie8 no-js ">
   <![endif]-->
   <!--[if IE 9]>
   <html lang="vi"
      class="ie9 no-js ">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="vi" class="">
         <!--<![endif]-->
         <head>
            <meta charset="utf-8"/>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta http-equiv="content-language" content="vi"/>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1" name="viewport"/>
            <meta name="csrf-token" content="AKqlhMYfrD8XpWwFmPz4VFPloGpG1UwZT1n3gz6s">
            <title>Phần mềm kế to&aacute;n &amp; Phần mềm ERP | Fast Accounting, Fast Business</title>
            <meta property="fb:admins" content=""/>
            <!-- <meta name="google-site-verification" content="DK1oK5EYMV5R8j6GcbdVp3T0afZvlY5njiACN-gj-X0" /> -->
            <meta name="google-site-verification" content="VfnQ5OUbawYqoMwGmgtwsi5kr9I3N62mHLFj_uGEOBc" />
            <!-- GLOBAL PLUGINS -->
            <!-- GLOBAL PLUGINS -->
            <!-- OTHER PLUGINS -->
            <style type="text/css">
               @media (min-width: 768px) {
               .nav-sol:before {
               background-color: rgba(0, 182, 188, 0.5) !important;
               border-radius: 0 !important;
               }
               .nav-sol:after {
               background-color: rgba(46, 104, 104, 0.82) !important;
               border-radius: 0 !important;
               }
               }
               @media (min-width: 768px) {
               .main>.container .nav-sol .container {
               width: 750px !important;
               }
               }
               @media (min-width: 1006px) {
               .main>.container .nav-sol .container {
               width: 970px !important;
               }
               }
               @media (min-width: 1260px) {
               .main>.container .nav-sol .container {
               width: 1170px !important;
               }
               }
            </style>
            <!-- END OTHER PLUGINS -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="{{ asset('css/app.min.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
            <!-- END THEME LAYOUT STYLES -->
            <link href="{{ asset('css/owl.carousel.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{ asset('css/owl.theme.default.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{ asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{ asset('css/fixedsticky.css')}}" rel="stylesheet" type="text/css"/>
            <script type="text/javascript" src="{{ asset('js/jQuery v1.11.3.js') }}" ></script>
            <style type="text/css">
               .article-content table {
               width: 100%;
               overflow-x: auto;
               display: inline-block;
               border: none;
               }
               .article-content table th, .article-content table td {
               border: 1px solid grey;
               }
               .skiptranslate.goog-te-gadget {
               color: transparent;
               }
               .skiptranslate.goog-te-gadget > div {
               color: #666;
               }
               .skiptranslate.goog-te-gadget > span {
               display: none;
               }
               @media (min-width: 768px) {
               .main-left {
               width: 250px;
               margin-left: 10px;
               }
               .main-right {
               float: right;
               width: 440px;
               margin-right: 10px;
               }
               }
               @media (min-width: 1006px) {
               .main-left {
               width: 250px;
               margin-left: 15px;
               }
               .main-right {
               width: 650px;
               margin-right: 15px;
               }
               }
               @media (min-width: 1260px) {
               .main-left {
               width: 300px;
               margin-left: 25px;
               }
               .main-right {
               width: 750px;
               margin-right: 25px;
               }
               }
               @media (max-width: 767px) {
               .top-head ul.pull-left li {
               text-indent: 0;
               width: 100%;
               }
               .contact-link {
               display: none !important;
               }
               #google_translate_element2 {
               height: 30px;
               }
               }
               .content-wrap .category .category-nav {
               padding-bottom: 5px;
               margin-bottom: 30px;
               background-color: #00b6bc;
               color: #FFF;
               line-height: 30px;
               padding-top: 5px;
               padding-left: 10px;
               position: relative;
               }
               .content-wrap .category .category-nav:before {
               content: '';
               background-color: #f7941d;
               height: 41px;
               width: 45px;
               display: inline-block;
               position: absolute;
               right: 0;
               top: 0;
               border-left: 10px solid #FFF;
               }
               .content-wrap .category .category-nav:after {
               content: '';
               background-color: #f7941d;
               height: 41px;
               width: 100px;
               display: inline-block;
               position: absolute;
               right: 45px;
               top: 0;
               border-left: 10px solid #FFF;
               }
               @media (max-width: 1005px) {
               .content-wrap .category .category-nav:before,.content-wrap .category .category-nav:after {
               display: none;
               }
               }
               .content-wrap .category .category-nav ul li.active a {
               color: #FFF;
               font-weight: bold;
               }
               .content-wrap .category .category-nav ul li a:hover {
               font-weight: bold;
               color: #FFF;
               }
               .article-content {
               text-align: unset;
               }
               @media (max-width: 767px) {
               .top-head ul.pull-left li a {
               color: #FFF;
               background-color: transparent;
               width: auto;
               }
               }
            </style>
            <link rel="icon" href="http://maxad.vn/images/max_icon.ico" type="image/x-icon" />
            <!--Start of Zopim Live Chat Script-->
            <script type="text/javascript">
               window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
               d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
               _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
               $.src="//v2.zopim.com/?48DB5piBfrXBfWalOUbHsHBvnP10AiwF";z.t=+new Date;$.
               type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
            </script>
            <!--End of Zopim Live Chat Script-->
            <!--Start of Google Analytics-->
            <script>
               (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
               (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
               m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
               })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
               
               ga('create', 'UA-92409807-1', 'auto');
               ga('send', 'pageview');
            </script>
            <!--End of Google Analytics-->
            <meta name="p:domain_verify" content="1637d583f4464ebd88d12ebd158eaf04"/>
         </head>
         <body class="page page-homepage">
            <div class="site-wrapper">
               <header id="top" class="header">
                  <style type="text/css">
                     .container>.navbar-header h1 {
                     margin-top: -13px;
                     }
                     @media (min-width: 768px) {
                     .container>.navbar-header h1 {
                     margin-top: 15px;
                     }
                     }
                     .swiper-slide:hover {
                     cursor: pointer;
                     }
                     .contact-link {
                     text-align: center;
                     font-size: 14px !important;
                     font-weight: normal !important;
                     display: inherit;
                     line-height: 14px;
                     }
                     .fixedsticky {
                     top: 0;
                     z-index: 4;
                     width: 100%;
                     }
                  </style>
                  @include('fontend.layouts.top-head')
               </header>
               @include('fontend.layouts.menu')
               @yield('content')
               @include('fontend.layouts.footer')
               
               
               
            </div>
            <!--Modals-->
            <div class="nav-support-fx">
               <ul>
                  <li>
                     <a href="#myModal-support-sale" data-toggle="modal">
                     <span>Tư vấn về phần mềm</span>
                     </a>
                  </li>
                  <li>
                     <a href="#myModal-support-it" data-toggle="modal">
                     <span>Tư vấn dịch vụ CNTT</span>
                     </a>
                  </li>
               </ul>
            </div>
            <!-- Modal Support-->
            <div class="modal fade" id="myModal-support-sale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                           aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tư vấn mua phần mềm</h4>
                     </div>
                     <div class="modal-body">
                        <dl class="dl-sbox">
                           <dd>
                              <div class="row row-sbox-skype">
                                 <div class="col-xs-12">
                                    <p class="sbox-tt">H&agrave; Nội</p>
                                    <ul>
                                       <li>
                                          <a href="skype:fhnsale01?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                       <li>
                                          <a href="skype:fastsalehn2?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                       <li>
                                          <a href="skype:fhnsale03?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                    </ul>
                                 </div>
                                 
                                
                              </div>
                           </dd>
                        </dl>
                     </div>
                     <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Tư vấn sử dụng phần mềm</h4>
                     </div>
                     <div class="modal-body">
                        <dl class="dl-sbox">
                           <dd>
                              <div class="row row-sbox-skype">
                                 <div class="col-xs-12">
                                    <p class="sbox-tt">H&agrave; Nội</p>
                                    <ul>
                                       <li>
                                          <a href="skype:fast_hn1?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                       <li>
                                          <a href="skype:fast_hn2?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                       <li>
                                          <a href="skype:fast_hn3?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                    </ul>
                                 </div>
                                 
                                 
                              </div>
                           </dd>
                        </dl>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade" id="myModal-support-it" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                           aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel2">Tư vấn dịch vụ CNTT</h4>
                     </div>
                     <div class="modal-body">
                        <dl class="dl-sbox">
                           <dd>
                              <div class="row row-sbox-skype">
                                 <div class="col-xs-12">
                                    <p class="sbox-tt">H&agrave; Nội</p>
                                    <ul>
                                       <li>
                                          <a href="skype:sunflowerdtn?chat"><img src="{{asset('images/chatbutton.png')}}"></a>
                                       </li>
                                    </ul>
                                 </div>
                                 
                              </div>
                           </dd>
                        </dl>
                     </div>
                  </div>
               </div>
            </div>
            <style type="text/css">
               @media (max-width: 768px) {
               #myModalPopup .modal-dialog {
               width: 90%;
               position: fixed;
               margin: 0;
               top: 30px;
               left: 5%;
               }
               #myModalPopup .modal-dialog img {
               width: 100%;
               }
               }
               @media (min-width: 768px) {
               #myModalPopup .modal-dialog {
               width: 650px;
               position: fixed;
               margin: 0;
               top: 50%;
               left: 50%;
               margin-left: -325px;
               margin-top: -200px;
               }
               }
               #myModalPopup .modal-dialog .modal-content {
               position: relative;
               }
               #myModalPopup .modal-dialog .modal-content .close {
               position: absolute;
               top: -15px;
               right: -15px;
               opacity: 1;
               }
            </style>
            <!--[if lt IE 9]>
            <script async='async' src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script async='async' src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <!--Google captcha-->
            <script type="text/javascript">
               var onloadGoogleCaptchaCallback = function () {
                   if (document.getElementById('contactBoxCaptcha')) {
                       grecaptcha.render('contactBoxCaptcha', {
                           'sitekey': ' 6Ldn7iQTAAAAAP31uluareBU8leIsehEp2awYh9K',
                           'theme': 'light'
                       });
                   }
                   if (document.getElementById('advisoryBoxCaptcha')) {
                       grecaptcha.render('advisoryBoxCaptcha', {
                           'sitekey': ' 6Ldn7iQTAAAAAP31uluareBU8leIsehEp2awYh9K',
                           'theme': 'light'
                       });
                   }
               };
            </script><!--Google captcha-->
            <script src="{{ asset('js/modernizr.js')}}"></script>
            <script src="{{ asset('js/bootstrap.min.js')}}"></script>
            <!--script async='async' defer src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
               integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
               crossorigin="anonymous"></script-->
            <script defer src="{{ asset('js/owl.carousel.js')}}"></script>
            <!-- OTHER PLUGINS -->
            <!-- END OTHER PLUGINS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <script src="{{ asset('js/app.min.js')}}"></script>
            <script>
               var banner = new Swiper('.banner-wrap .swiper-container', {
                   pagination: '.banner-wrap .swiper-pagination',
                   autoplay: 4000,
                   paginationClickable: true,
                   autoplayDisableOnInteraction: false,
                   preventClicks: false,
                   preventClicksPropagation: false,
               });
               $(document).ready(function () {
                   if ($('.nav-sol').length) {
                       $('.main').addClass('main-nav');
                   }
                   
               });
               
               
            </script>
            <!-- END THEME LAYOUT SCRIPTS -->
            <!-- JS INIT -->
            <script>
               $(document).ready(function () {
                                   var list0 = new Swiper('#top-gp-0 .swiper-container', {
                           pagination: '#top-gp-0 .swiper-pagination',
                           paginationClickable: true,
                           slidesPerView: 3,
                           spaceBetween: 30,
                           breakpoints: {
                               640: {
                                   slidesPerView: 2,
                                   spaceBetween: 20
                               },
                               320: {
                                   slidesPerView: 1,
                                   spaceBetween: 10
                               }
                           },
                           preventClicks: false,
                           preventClicksPropagation: false
                       });
                               var list1 = new Swiper('#top-gp-1 .swiper-container', {
                           pagination: '#top-gp-1 .swiper-pagination',
                           paginationClickable: true,
                           slidesPerView: 3,
                           spaceBetween: 30,
                           breakpoints: {
                               640: {
                                   slidesPerView: 2,
                                   spaceBetween: 20
                               },
                               320: {
                                   slidesPerView: 1,
                                   spaceBetween: 10
                               }
                           },
                           preventClicks: false,
                           preventClicksPropagation: false
                       });
                               var list2 = new Swiper('#top-gp-2 .swiper-container', {
                           pagination: '#top-gp-2 .swiper-pagination',
                           paginationClickable: true,
                           slidesPerView: 3,
                           spaceBetween: 30,
                           breakpoints: {
                               640: {
                                   slidesPerView: 2,
                                   spaceBetween: 20
                               },
                               320: {
                                   slidesPerView: 1,
                                   spaceBetween: 10
                               }
                           },
                           preventClicks: false,
                           preventClicksPropagation: false
                       });
                               $('.list-wrap').hide();
                   $('.list-wrap.active').show();
                   $('.nav-sol a').bind('click', function (e) {
                       $('.nav-sol a').removeClass('active');
                       $(this).addClass('active');
                       var id = $(this).attr('list-wrap');
                       $('.list-wrap').hide();
                       $('#' + id).show();
                   })
               });
            </script>
            <!-- JS INIT -->
            <!--Website Translator plugin-->
            <script>
               function GTranslateFireEvent(a, b) {
                   try {
                       if (document.createEvent) {
                           var c = document.createEvent("HTMLEvents");
                           c.initEvent(b, true, true);
                           a.dispatchEvent(c)
                       } else {
                           var c = document.createEventObject();
                           a.fireEvent('on' + b, c)
                       }
                   } catch (e) {
                   }
               }
               function doTranslate(a) {
                   if (a.value)a = a.value;
                   if (a == '')return;
                   var b = a.split('|')[1];
                   var c;
                   var d = document.getElementsByTagName('select');
                   for (var i = 0; i < d.length; i++)if (d[i].className == 'goog-te-combo')c = d[i];
                   if (document.getElementById('google_translate_element2') == null || document.getElementById('google_translate_element2').innerHTML.length == 0 || c.length == 0 || c.innerHTML.length == 0) {
                       setTimeout(function () {
                           doTranslate(a);
                       }, 500)
                   } else {
                       c.value = b;
                       GTranslateFireEvent(c, 'change');
                       GTranslateFireEvent(c, 'change');
                   }
               }
               
            </script>
            <script type="text/javascript">
               function googleTranslateElementInit() {
                   new google.translate.TranslateElement({
                       pageLanguage: 'vi',
                       includedLanguages: 'vi,en,zh-CN,de,fr,ja,ko,ru',
                       multilanguagePage: false,
                       autoDisplay: false
                   }, 'google_translate_element2');
               }
            </script>
            <script async='async' defer type="text/javascript"
               src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <!-- Google Code dành cho Thẻ tiếp thị lại -->
            <script type="text/javascript">
               /* <![CDATA[ */
               var google_conversion_id = 866575527;
               var google_custom_params = window.google_tag_params;
               var google_remarketing_only = true;
               /* ]]> */
               $(document).ready(function () {
                   var our_customers = $('.slider-customer-of-us.owl-carousel');
                   our_customers.owlCarousel({
                       loop: true,
                       margin: 15,
                       nav: true,
                       navText: ["<i class='fa fa-angle-down' aria-hidden='true'></i>", "<i class='fa fa-angle-down' aria-hidden='true'></i>"],
                       responsiveClass: true,
                       responsive: {
                           0: {
                               items: 2,
                               nav: false,
                               slideBy: 2
                           },
                           540: {
                               items: 4,
                               nav: false,
                               slideBy: 4
                           },
                           600: {
                               items: 6,
                               nav: false,
                               slideBy: 6
                           },
                           1000: {
                               items: 8,
                               nav: true,
                               slideBy: 8
                           }
                       },
                       autoplay:true,
                       autoplayHoverPause:true,
                       rtl:true
                   });
                   
                   setInterval(function(){ $('.slider-customer-of-us').find('.owl-next').trigger('click'); }, 5000);
               
                   $('.slider-detail-product.owl-carousel').owlCarousel({
                       items: 1,
                       margin: 10,
                       nav: true,
                       navText: ["<i class='fa fa-angle-down' aria-hidden='true'></i>", "<i class='fa fa-angle-down' aria-hidden='true'></i>"],
                       autoHeight: true
                   });
               });
            </script>
            <script type="text/javascript" async='async' src="//www.googleadservices.com/pagead/conversion.js"></script>
            <script defer src="{{ asset('js/jquery.lazyload.min.js') }}"></script>
            <script defer src="{{ asset('js/fixedsticky.js') }}"></script>
            <script type="text/javascript">
               jQuery(function ($) {
                   $(function () {
                       $("img.lazy").lazyload({
                           effect : "fadeIn"
                       });
                       $('.fixedsticky').fixedsticky();
                       $.ajaxSetup({
                           headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           }
                       });
                   });
               });
            </script>
            <noscript>
               <div style="display:inline;">
                  <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/866575527/?guid=ON&amp;script=0"/>
               </div>
            </noscript>
         </body>
      </html>