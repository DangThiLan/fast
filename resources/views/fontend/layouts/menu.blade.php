<header id="menu" class="header fixedsticky">
                  <nav class="navbar navbar-default">
                     <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                              data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           </button>
                           <a class="navbar-brand" href="/home"
                              title="Phần mềm kế toán">
                              <h1><img style="width: 140px;" src="http://fast.com.vn/images/logo.svg" alt="Phần mềm kế toán"></h1>
                           </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-menu">
                              <li class=" menu-item-has-children dropdown ">
                                 <a class="" href="javascript:void(0);" title="C&ocirc;ng ty"><span>Công ty</span></a>
                                 <ul class="dropdown-menu sub-menu">
                                    <li class="  "><a class="" href="{{ url('/home/gioithieu') }}" title="Giới thiệu về FAST"><span>Giới thiệu về MBSOFT</span></a></li>
                                    <li class="  "><a class="" href="{{ url('/home/cacgiaithuong') }}" title="các giải thưởng"><span>các giải thưởng</span></a></li>
                                 </ul>
                              </li>
                              <li class=" menu-item-has-children dropdown ">
                                 <a class="" href="detail-online.html" title="Sản phẩm"><span>Sản phẩm</span></a>
                                 <ul class="dropdown-menu sub-menu">
                                    <li class=" menu-item-has-children dropdown ">
                                       <a class="" href="javascript:void(0);" title="các sản phẩm"><span>các sản phẩm</span></a>
                                       <ul class="dropdown-menu sub-menu">
                                          <li class="  "><a class="" href="{{ url('/home/tinhnangchung') }}" title="Fast Business Online"><span>Fast Business Online</span></a>
                                          <li class="  "><a class="" href="{{ url('/home/tinhnangchung') }}" title="Fast CRM Online"><span>Fast CRM Online</span></a></li>
                                       </ul>
                                    </li>
                                    <li class=" menu-item-has-children dropdown "></li>
                                 </ul>
                              </li>
                              <li class=" menu-item-has-children dropdown ">
                                 <a class="" href="#" title="Dịch vụ"><span>Dịch vụ</span></a>
                                 <ul class="dropdown-menu sub-menu">
                                    <li class="  "><a class="" href="{{ url('/home/tuvanungdung') }}" title="Tư vấn ứng dụng"><span>Tư vấn ứng dụng</span></a></li>
                                    <li class="  "><a class="" href="{{ url('/home/tuvanhotro') }}" title="Tư vấn, hỗ trợ sử dụng"><span>Tư vấn, hỗ trợ sử dụng</span></a></li>
                                    <li class="  "><a class="" href="{{ url('/home/tuvancntt') }}" title="Dịch vụ công nghệ thông tin"><span>Dịch vụ công nghệ thông tin</span></a></li>
                                 </ul>
                              </li>
                              <li class=" menu-item-has-children dropdown ">
                                 <a class="" href="bang-gia-sp.html" title="Bảng gi&aacute;"><span>Bảng giá</span></a>
                                 <ul class="dropdown-menu sub-menu">
                                    <li class=" menu-item-has-children dropdown ">
                                       <a class="" href="javascript:void(0);" title="Giải ph&aacute;p vừa v&agrave; lớn"><span>Giá sản phẩm</span></a>
                                       <ul class="dropdown-menu sub-menu">
                                          <li class="  "><a class="" href="{{ url('/home/banggiasp1') }}" title="Fast Business Online"><span>Fast Business Online</span></a></li>
                                          <li class="  "><a class="" href="{{ url('/home/banggiasp2') }}" title="Fast Business"><span>Fast Business</span></a></li>
                                       </ul>
                                    </li>
                                    
                                 </ul>
                              </li>
                              <li class=" menu-item-has-children dropdown ">
                                 <a class="" href="download-dulieu.html" title="Download"><span>Download</span></a>
                                 <ul class="dropdown-menu sub-menu">
                                    <li class="  "><a class="" href="{{ url('home/download') }}" title="download"><span>Download</span></a></li>
                                    
                                 </ul>
                              </li>
                              <li class=" menu-item-has-children dropdown ">
                                 <a class="" href="tuyen-dung.html" title="Tuyển dụng"><span>Tuyển dụng</span></a>
                                 <ul class="dropdown-menu sub-menu">
                                    <li class="  "><a class="" href="{{ url('/home/tuyendung') }}" title="thông tin tuyển dụng"><span>Thông tin tuyển dụng</span></a></li>
                                    
                                 </ul>
                              </li>
                              <li class="  "><a class="" href="{{ url('home/lienhe') }}" title="liên hệ"><span>liên hệ</span></a></li>
                           </ul>
                           <form class="navbar-form navbar-left visible-xs-block" action="/tim-kiem" accept-charset="UTF-8" method="GET">
                              <div class="form-group">
                                 <input type="text" class="form-control" placeholder="Nhập từ kh&oacute;a t&igrave;m kiếm" name="k" value="" style="position: relative; z-index: 1; padding-right: 40px;">
                              </div>
                              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                           </form>
                           <ul class="nav navbar-nav navbar-right">
                              <li><a target="_blank" href="https://www.facebook.com"><i class="fa fa-facebook-f"></i> Facebook</a></li>
                              <li><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube"></i> Youtube</a></li>
                           </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                     </div>
                     <!-- /.container-fluid -->
                  </nav>
               </header>