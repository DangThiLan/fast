@extends('fontend.layouts.index')
@section('content')

<main class="main">
   <div class="container">
      <div class="container">
         <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">﻿Dịch vụ</h3>
                              <ul>
                                 <li class="active">
                                    <a title="Tư vấn ứng dụng"
                                       href="{{ url('home/tuvanungdung') }}">Tư vấn ứng dụng</a>
                                 </li>
                                 <li class="">
                                    <a title="Tư vấn, hỗ trợ sử dụng"
                                       href="{{ url('home/tuvanhotro') }}">Tư vấn, hỗ trợ sử dụng</a>
                                 </li>
                                 <li class="">
                                    <a title="Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin"
                                       href="{{ url('home/tuvancntt') }}">Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin</a>
                                 </li>
                              </ul>
                           </nav>
                        </div>
         <div class="main-right">
            <div class="content-detail">
               <h1 class="title-detail">Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; HTTT QLDN</h1>
               <div class="detail-top">
                  Ng&agrave;y đăng: <span class="color">2016-08-02 20:38:43</span> - Ng&agrave;y cập nhật: <span class="color">2017-02-18 08:38:36</span>
                  - Số lần xem: <span class="color">1976</span>
               </div>
               <div class="detail-rating">
                  <div class="rating-wrap ml0">
                     <script>
                        $(document).ready(function () {
                            var text_rate = $('.rating-text-txt').html();
                            var rated = false;
                            $('input[name="rating"]').on('click', function (e) {
                                e.preventDefault();
                                var form_data = $(this).val();
                        
                                if (!rated) {
                                    $('.rating-text-txt')
                                        .hide()
                                        .addClass('rating-text-loading')
                                        .html('Đang cập nhật...')
                                        .fadeIn();
                        
                                    $.ajax({
                                        url: '/global-actions/rating/post/169',
                                        type: 'POST',
                                        data: {
                                            value: form_data
                                        },
                                        cache: false,
                                        complete: function (data) {
                                            var _data = data.responseJSON;
                                            var total = _data.data.total,
                                                ratings_average = parseFloat(_data.data.ratings_average).toFixed(1);
                                            if (!_data.error) {
                                                rated = true;
                                                $('.rating-text-txt')
                                                    .hide()
                                                    .removeClass('rating-text-loading')
                                                    .text('Cám ơn bạn đã cho điểm!')
                                                    .fadeIn();
                                                setTimeout(function () {
                                                    text_rate = 'Điểm: <span class="color">' + ratings_average + '/5</span> (' + total + ' phiếu)';
                                                    $('.rating-text-txt')
                                                        .hide()
                                                        .html(text_rate)
                                                        .fadeIn();
                                                }, 2000);
                                            }
                                        }
                                    });
                                } else {
                                    setTimeout(function () {
                                        $('.rating-text-txt')
                                            .hide()
                                            .removeClass('rating-text-loading')
                                            .html('Bạn đã cho điểm bài này rồi!')
                                            .fadeIn();
                                    }, 1000);
                                    setTimeout(function () {
                                        $('.rating-text-txt')
                                            .hide()
                                            .html(text_rate)
                                            .fadeIn();
                                    }, 3000);
                                }
                            });
                        });
                     </script>
                     <div class="rating-text">
                        <span class="rating-text-txt">Điểm: <span class="color">5/5</span> (3 phiếu)</span>
                     </div>
                     <span class="rating">
                     <input id="rating5" type="radio" name="rating" value="5" checked>
                     <label for="rating5">5</label>
                     <input id="rating4" type="radio" name="rating" value="4" >
                     <label for="rating4">4</label>
                     <input id="rating3" type="radio" name="rating" value="3" >
                     <label for="rating3">3</label>
                     <input id="rating2" type="radio" name="rating" value="2" >
                     <label for="rating2">2</label>
                     <input id="rating1" type="radio" name="rating" value="1" >
                     <label for="rating1">1</label>
                     </span>
                  </div>
               </div>
               <div class="social-bxcount">
                  <ul class="social">
                     <li>
                        Chia sẻ
                     </li>
                     <li>
                        <div class="facebook" style="display: inline-block;">
                           <div class="fb-like"
                              data-href="#"
                              data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                           <div class="fb-share-button" data-href="#" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                              <a class="fb-xfbml-parse-ignore" target="_blank" href="#&src=sdkpreparse">Share</a>
                           </div>
                           <div id="fb-root"></div>
                           <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
                              fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));
                           </script>
                        </div>
                     </li>
                     <li>
                        <!-- Place this tag in your head or just before your close body tag. -->
                        <script src="https://apis.google.com/js/platform.js" async defer></script>
                        <!-- Place this tag where you want the share button to render. -->
                        <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                     </li>
                  </ul>
               </div>
               <article class="article-content">
                  <div class="article-content">
                     <div>Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; hệ thống th&ocirc;ng tin quản l&yacute; doanh nghiệp cho kh&aacute;ch h&agrave;ng gồm c&aacute;c bước sau:</div>
                     <div>
                        <ol start="3"></ol>
                        <ol>
                           <li><strong><a href="#" target="_blank" title="Tư vấn lựa chọn sản phẩm, giải pháp"><span style="color:#008080">Tư vấn lựa chọn sản phẩm, giải ph&aacute;p</span></a></strong></li>
                           <li><strong><a href="#" target="_blank" title="Khảo sát, phân tích yêu cầu và thiết kế sản phẩm"><span style="color:#008080">Khảo s&aacute;t, ph&acirc;n t&iacute;ch y&ecirc;u cầu v&agrave; thiết kế sản phẩm</span></a></strong></li>
                           <li><strong><a href="#" target="_blank" title="Cài đặt, đào tạo, chuyển đổi số liệu và triển khai sử dụng"><span style="color:#008080">C&agrave;i đặt, đ&agrave;o tạo, chuyển đổi số liệu v&agrave; triển khai sử dụng</span></a></strong></li>
                           <li><strong><a href="#" title="Bảo hành, bảo trì và hỗ trợ sử dụng"><span style="color:#008080">Bảo h&agrave;nh, bảo tr&igrave; v&agrave; hỗ trợ sử dụng</span></a></strong></li>
                           <li><strong><a href="#" target="_blank" title="Nâng cấp sản phẩm, mở rộng ứng dụng"><span style="color:#008080">N&acirc;ng cấp sản phẩm, mở rộng ứng dụng.</span></a></strong></li>
                        </ol>
                     </div>
                  </div>
               </article>
               <div class="tags-wrap">
                  <span>Tags:</span>
                  <a href="#" title="Tư vấn ứng dụng phần mềm"
                     style="color: #00b6bc;">Tư vấn ứng dụng phần mềm</a>,
                  <a href="/tags?k=+Hệ+thống+th&ocirc;ng+tin+quản+l&yacute;+doanh+nghiệp" title=" Hệ thống th&ocirc;ng tin quản l&yacute; doanh nghiệp"
                     style="color: #00b6bc;"> Hệ thống th&ocirc;ng tin quản l&yacute; doanh nghiệp</a>,
                  <a href="/tags?k=+HTTTQLDN" title=" HTTTQLDN"
                     style="color: #00b6bc;"> HTTTQLDN</a>,
               </div>
            </div>
            <div class="article-relative-list">
               <h4 class="article-tt-other">B&agrave;i li&ecirc;n quan</h4>
               
            </div>
            <div class="article-comment">
               <style>
                  #comments .media-list.show-all-parent .media.hidden:not(.media2) {
                  display : block !important;
                  }
                  #comments .media-list .group.show-all .media.hidden {
                  display : block !important;
                  }
               </style>
               <div class="facebook-comment">
                  <h4 class="article-tt-other">Gửi b&igrave;nh luận</h4>
                  <form method="POST" id="form-comment" accept-charset="UTF-8" action="/global-actions/create-comment/post/169">
                     <input type="hidden" name="_token" value="kTxcukEzh9Ygf3gmR8ruNj96g93B2gYu4zNYY5FL">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="form-group">
                              <input type="text" name="name" class="form-control" value=""
                                 placeholder="Họ v&agrave; t&ecirc;n *" required autocomplete="off">
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <input type="text" name="email" class="form-control" value=""
                                 placeholder="Email *" required autocomplete="off">
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <input type="text" name="phone" class="form-control" value=""
                                 placeholder="Số điện thoại *" required autocomplete="off">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 text-right">
                           <button type="submit" class="btn btn-primary">Gửi b&igrave;nh luận</button>
                        </div>
                     </div>
                  </form>
                  <h4 class="article-tt-other">2 B&igrave;nh luận</h4>
                  <div class="row">
                     <div class="col-md-12">
                        <div id="comments">
                           <div class="media-list">
                              <script>
                                 function voteComment(id, type) {
                                     var el = $('[data-vote-comment=' + id + ']');
                                     var num = el.find('.vote-comment-txt').attr('vote-comment-count');
                                     num = +num + 1;
                                     if (type === 'down') {
                                         $data = {type: 'dislike'};
                                 
                                         el.find('.hasTip-down').text('(' + num + ')');
                                         el.find('.votedown-btn').addClass('active');
                                     } else {
                                         $data = {type: 'like'};
                                         el.find('.hasTip-up').text('(' + num + ')');
                                         el.find('.voteup-btn').addClass('active');
                                     }
                                     $.ajax({
                                         url: '/global-actions/rating-comment/' + id,
                                         type: 'POST',
                                         data: $data,
                                         cache: false,
                                         complete: function (data) {
                                             var _data = data.responseJSON;
                                             if (!_data.error) {
                                                 var like_count = _data.data.like_count,
                                                         dislike_count = _data.data.dislike_count;
                                 
                                                 el.find('.hasTip-up').text('(' + like_count + ')');
                                                 el.find('.hasTip-down').text('(' + dislike_count + ')');
                                 
                                                 if (type === 'down') {
                                                     el.find('.votedown-btn').addClass('active');
                                                 } else {
                                                     el.find('.voteup-btn').addClass('active');
                                                 }
                                 
                                                 el.find('.hasTip').removeAttr('href');
                                             }
                                         }
                                     });
                                 }
                                 $(document).ready(function () {
                                     $('body').on('click', '.fbs-vote-comment > .media-heading-toggle', function (event) {
                                         var $current = $(this);
                                         var $target = $($current.attr('href'));
                                 
                                         $target.find('textarea').html('@' + $current.attr('data-name') + ': ');
                                     });
                                 });
                              </script>
                              <div class="media  ">
                                 <div class="media-left">
                                    <a href="#">
                                    <img src="{{ url('images/ad1.png') }}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       Nguyễn Thị Dung
                                       <span>2018-02-22 11:36:06</span>
                                       <span data-vote-comment="1635" class="fbs-vote-comment">
                                       <a role="button" data-toggle="collapse" href="#comments-1635" aria-expanded="false"
                                          aria-controls="comments-1635" class="media-heading-toggle"
                                          data-name="Nguyễn Thị Dung">open</a>
                                       <a href="javascript:voteComment(1635, 'up')" title="Th&iacute;ch"
                                          class="voteup-btn hasTip">
                                       <i vote-comment-count="0"
                                          class="vote-comment-txt hasTip-up">(0)</i>
                                       </a>
                                       <a href="javascript:voteComment(1635, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                          class="votedown-btn hasTip">
                                       <i vote-comment-count="0"
                                          class="vote-comment-txt hasTip-down">(0)</i>
                                       </a>
                                       </span>
                                    </h4>
                                    <div class="clearfix">Tư vấn phần mềm</div>
                                 </div>
                              </div>
                              <div class="group">
                                 <div class="media media2 ">
                                    <div class="media-left">
                                       <a href="#">
                                       <img src="{{ url('images/ad1.png') }}" alt="" class="media-object">
                                       </a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading">
                                          C&Ocirc;NG TY FAST
                                          <span>2018-02-23 08:23:30</span>
                                          <span data-vote-comment="1637" class="fbs-vote-comment">
                                          <a href="javascript:voteComment(1637, 'up')" title="Th&iacute;ch"
                                             class="voteup-btn hasTip">
                                          <i vote-comment-count="0"
                                             class="vote-comment-txt hasTip-up">(0)</i>
                                          </a>
                                          <a href="javascript:voteComment(1637, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                             class="votedown-btn hasTip">
                                          <i vote-comment-count="0"
                                             class="vote-comment-txt hasTip-down">(0)</i>
                                          </a>
                                          </span>
                                       </h4>
                                       <div class="clearfix">@Nguyễn Thị Dung: Chào chị Dung,<br />
                                          <br />
                                          Cảm ơn chị đã quan tâm đến phần mềm FAST. FAST sẽ liên hệ tư vấn trực tiếp nhé.<br />
                                          Trân trọng.<br />
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="collapse" id="comments-1635">
                                 <form method="POST" id="form-comment" accept-charset="UTF-8"
                                    action="/global-actions/create-comment/post/169">
                                    <input type="hidden" name="_token" value="kTxcukEzh9Ygf3gmR8ruNj96g93B2gYu4zNYY5FL">
                                    <input type="hidden" name="parent_id" value="1635">
                                    <div class="media media2">
                                       <div class="form-group">
                                          <input type="text" name="name" class="form-control" value=""
                                             placeholder="Họ v&agrave; t&ecirc;n *" required
                                             autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <input type="text" name="email" class="form-control" value=""
                                             placeholder="Email *" required autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <input type="text" name="phone" class="form-control" value=""
                                             placeholder="Số điện thoại *" required autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <textarea class="form-control" rows="5" name="content"
                                             placeholder="Nội dung"></textarea>
                                       </div>
                                       <div class="text-right">
                                          <button type="submit"
                                             class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="media  ">
                                 <div class="media-left">
                                    <a href="#">
                                    <img src="{{ url('images/ad1.png') }}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       Nguyễn Thanh T&ugrave;ng
                                       <span>2018-01-05 15:21:00</span>
                                       <span data-vote-comment="1506" class="fbs-vote-comment">
                                       <a role="button" data-toggle="collapse" href="#comments-1506" aria-expanded="false"
                                          aria-controls="comments-1506" class="media-heading-toggle"
                                          data-name="Nguyễn Thanh T&ugrave;ng">open</a>
                                       <a href="javascript:voteComment(1506, 'up')" title="Th&iacute;ch"
                                          class="voteup-btn hasTip">
                                       <i vote-comment-count="0"
                                          class="vote-comment-txt hasTip-up">(0)</i>
                                       </a>
                                       <a href="javascript:voteComment(1506, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                          class="votedown-btn hasTip">
                                       <i vote-comment-count="0"
                                          class="vote-comment-txt hasTip-down">(0)</i>
                                       </a>
                                       </span>
                                    </h4>
                                    <div class="clearfix">tư vấn giải pháp phần mềm </div>
                                 </div>
                              </div>
                              <div class="group">
                                 <div class="media media2 ">
                                    <div class="media-left">
                                       <a href="#">
                                       <img src="{{ url('images/ad1.png') }}" alt="" class="media-object">
                                       </a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading">
                                          C&Ocirc;NG TY FAST
                                          <span>2018-01-06 10:47:20</span>
                                          <span data-vote-comment="1518" class="fbs-vote-comment">
                                          <a href="javascript:voteComment(1518, 'up')" title="Th&iacute;ch"
                                             class="voteup-btn hasTip">
                                          <i vote-comment-count="0"
                                             class="vote-comment-txt hasTip-up">(0)</i>
                                          </a>
                                          <a href="javascript:voteComment(1518, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                             class="votedown-btn hasTip">
                                          <i vote-comment-count="0"
                                             class="vote-comment-txt hasTip-down">(0)</i>
                                          </a>
                                          </span>
                                       </h4>
                                       <div class="clearfix">Chào anh Tùng,<br />
                                          <br />
                                          Cảm ơn anh đã quan tâm đến phần mềm FAST. FAST sẽ liên hệ tư vấn trực tiếp nhé.<br />
                                          Trân trọng.<br />
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="collapse" id="comments-1506">
                                 <form method="POST" id="form-comment" accept-charset="UTF-8"
                                    action="/global-actions/create-comment/post/169">
                                    <input type="hidden" name="_token" value="kTxcukEzh9Ygf3gmR8ruNj96g93B2gYu4zNYY5FL">
                                    <input type="hidden" name="parent_id" value="1506">
                                    <div class="media media2">
                                       <div class="form-group">
                                          <input type="text" name="name" class="form-control" value=""
                                             placeholder="Họ v&agrave; t&ecirc;n *" required
                                             autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <input type="text" name="email" class="form-control" value=""
                                             placeholder="Email *" required autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <input type="text" name="phone" class="form-control" value=""
                                             placeholder="Số điện thoại *" required autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <textarea class="form-control" rows="5" name="content"
                                             placeholder="Nội dung"></textarea>
                                       </div>
                                       <div class="text-right">
                                          <button type="submit"
                                             class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</main>
@endsection