@extends('fontend.layouts.index')
@section('content')
	<main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">﻿Dịch vụ</h3>
                              <ul>
                                 <li class="active">
                                    <a title="Tư vấn ứng dụng"
                                       href="{{ url('home/tuvanungdung') }}">Tư vấn ứng dụng</a>
                                 </li>
                                 <li class="">
                                    <a title="Tư vấn, hỗ trợ sử dụng"
                                       href="{{ url('home/tuvanhotro') }}">Tư vấn, hỗ trợ sử dụng</a>
                                 </li>
                                 <li class="">
                                    <a title="Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin"
                                       href="{{ url('home/tuvancntt') }}">Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin</a>
                                 </li>
                              </ul>
                           </nav>
                        </div>
                        <div class="main-right">
                           <div class="list-category">
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                       title="Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; HTTT QLDN" class="img">
                                    <img src="{{ url('images/vp-hn-10.png') }}" alt="Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; HTTT QLDN">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                          title="Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; HTTT QLDN">Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; HTTT QLDN</a>
                                    </p>
                                    <div class="summary">Quy tr&igrave;nh tư vấn ứng dụng phần mềm v&agrave; hệ thống th&ocirc;ng tin quản l&yacute; doanh nghiệp cho kh&aacute;ch h&agrave;ng gồm c&aacute;c bước sau</div>
                                    <div class="analytics">
                                       <span>Ngày đăng: 2016-08-02 20:38:43</span>
                                       <span>Ngày cập nhật: 2018-04-18 11:57:34</span>
                                       <span>Số lần xem: 1970</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                       title="Tư vấn lựa chọn sản phẩm, giải ph&aacute;p" class="img">
                                    <img src="{{ url('images/vp-hn-10.png') }}" alt="Tư vấn lựa chọn sản phẩm, giải ph&aacute;p">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                          title="Tư vấn lựa chọn sản phẩm, giải ph&aacute;p">Tư vấn lựa chọn sản phẩm, giải ph&aacute;p</a>
                                    </p>
                                    <div class="summary">Việc đi t&igrave;m một nh&agrave; cung cấp giải ph&aacute;p phần mềm quản l&yacute; kh&ocirc;ng đơn giản như đi t&igrave;m một nh&agrave; cung cấp c&aacute;c sản phẩm hữu h&igrave;nh. L&agrave;m sao để qu&aacute; tr&igrave;nh lựa chọn đ&aacute;p ứng được nhu cầu của doanh nghiệp, ph&ugrave; hợp với những điều kiện cụ thể của doanh nghiệp.</div>
                                    <div class="analytics">
                                       <span>Ngày đăng: 2016-08-02 20:57:00</span>
                                       <span>Ngày cập nhật: 2018-04-18 17:06:05</span>
                                       <span>Số lần xem: 1878</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                       title="Khảo s&aacute;t, ph&acirc;n t&iacute;ch y&ecirc;u cầu v&agrave; thiết kế sản phẩm" class="img">
                                    <img src="{{ url('images/vp-hn-10.png') }}" alt="Khảo s&aacute;t, ph&acirc;n t&iacute;ch y&ecirc;u cầu v&agrave; thiết kế sản phẩm">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                          title="Khảo s&aacute;t, ph&acirc;n t&iacute;ch y&ecirc;u cầu v&agrave; thiết kế sản phẩm">Khảo s&aacute;t, ph&acirc;n t&iacute;ch y&ecirc;u cầu v&agrave; thiết kế sản phẩm</a>
                                    </p>
                                    <div class="summary">Khảo s&aacute;t chi tiết y&ecirc;u cầu v&agrave; thiết kế sản phẩm nằm trong chuỗi cung ứng dịch vụ của FAST. Đ&acirc;y l&agrave; kh&acirc;u cực kỳ quan trọng trước khi đưa ra một sản phẩm ph&ugrave; hợp với kh&aacute;ch h&agrave;ng.</div>
                                    <div class="analytics">
                                       <span>Ngày đăng: 2016-08-02 20:51:33</span>
                                       <span>Ngày cập nhật: 2018-04-19 09:46:15</span>
                                       <span>Số lần xem: 2221</span>
                                    </div>
                                 </div>
                              </div>
                              
                             
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection