@extends('fontend.layouts.index')
@section('content')
<main class="main">
   <div class="container">
      <div class="container">
         <div class="main-left">
            <nav class="menu-left aside-left">
               <h3 class="title-left">﻿Dịch vụ</h3>
               <ul>
                  <li class="">
                     <a title="Tư vấn ứng dụng"
                        href="{{ url('home/tuvanungdung') }}">Tư vấn ứng dụng</a>
                  </li>
                  <li class="active">
                     <a title="Tư vấn, hỗ trợ sử dụng"
                        href="{{ url('home/tuvanhotro') }}">Tư vấn, hỗ trợ sử dụng</a>
                  </li>
                  <li class="">
                     <a title="Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin"
                        href="{{ url('home/tuvancntt') }}">Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin</a>
                  </li>
               </ul>
            </nav>
         </div>
         <div class="main-right">
            <div class="content-detail">
               <h1 class="title-detail"> Bảo h&agrave;nh, bảo tr&igrave; v&agrave; hỗ trợ sử dụng</h1>
               <div class="detail-top">
                  Ng&agrave;y đăng: <span class="color">2016-08-02 20:47:13</span> - Ng&agrave;y cập nhật: <span class="color">2017-01-06 17:42:49</span>
                  - Số lần xem: <span class="color">9270</span>
               </div>
               <div class="detail-rating">
                  <div class="rating-wrap ml0">
                     <script>
                        $(document).ready(function () {
                            var text_rate = $('.rating-text-txt').html();
                            var rated = false;
                            $('input[name="rating"]').on('click', function (e) {
                                e.preventDefault();
                                var form_data = $(this).val();
                        
                                if (!rated) {
                                    $('.rating-text-txt')
                                        .hide()
                                        .addClass('rating-text-loading')
                                        .html('Đang cập nhật...')
                                        .fadeIn();
                        
                                    $.ajax({
                                        url: '/global-actions/rating/post/172',
                                        type: 'POST',
                                        data: {
                                            value: form_data
                                        },
                                        cache: false,
                                        complete: function (data) {
                                            var _data = data.responseJSON;
                                            var total = _data.data.total,
                                                ratings_average = parseFloat(_data.data.ratings_average).toFixed(1);
                                            if (!_data.error) {
                                                rated = true;
                                                $('.rating-text-txt')
                                                    .hide()
                                                    .removeClass('rating-text-loading')
                                                    .text('Cám ơn bạn đã cho điểm!')
                                                    .fadeIn();
                                                setTimeout(function () {
                                                    text_rate = 'Điểm: <span class="color">' + ratings_average + '/5</span> (' + total + ' phiếu)';
                                                    $('.rating-text-txt')
                                                        .hide()
                                                        .html(text_rate)
                                                        .fadeIn();
                                                }, 2000);
                                            }
                                        }
                                    });
                                } else {
                                    setTimeout(function () {
                                        $('.rating-text-txt')
                                            .hide()
                                            .removeClass('rating-text-loading')
                                            .html('Bạn đã cho điểm bài này rồi!')
                                            .fadeIn();
                                    }, 1000);
                                    setTimeout(function () {
                                        $('.rating-text-txt')
                                            .hide()
                                            .html(text_rate)
                                            .fadeIn();
                                    }, 3000);
                                }
                            });
                        });
                     </script>
                     <div class="rating-text">
                        <span class="rating-text-txt">Điểm: <span class="color">3/5</span> (2 phiếu)</span>
                     </div>
                     <span class="rating">
                     <input id="rating5" type="radio" name="rating" value="5" >
                     <label for="rating5">5</label>
                     <input id="rating4" type="radio" name="rating" value="4" >
                     <label for="rating4">4</label>
                     <input id="rating3" type="radio" name="rating" value="3" checked>
                     <label for="rating3">3</label>
                     <input id="rating2" type="radio" name="rating" value="2" >
                     <label for="rating2">2</label>
                     <input id="rating1" type="radio" name="rating" value="1" >
                     <label for="rating1">1</label>
                     </span>
                  </div>
               </div>
               <div class="social-bxcount">
                  <ul class="social">
                     <li>
                        Chia sẻ
                     </li>
                     <li>
                        <div class="facebook" style="display: inline-block;">
                           <div class="fb-like"
                              data-href="{{ url('home/tuvanhotro') }}"
                              data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                           <div class="fb-share-button" data-href="{{ url('home/tuvanhotro') }}" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                              <a class="fb-xfbml-parse-ignore" target="_blank" href="http://fast.com.vn/tu-van-ho-tro-su-dung&src=sdkpreparse">Share</a>
                           </div>
                           <div id="fb-root"></div>
                           <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
                              fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));
                           </script>
                        </div>
                     </li>
                     <li>
                        <!-- Place this tag in your head or just before your close body tag. -->
                        <script src="https://apis.google.com/js/platform.js" async defer></script>
                        <!-- Place this tag where you want the share button to render. -->
                        <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                     </li>
                  </ul>
               </div>
               <div class="article-content">
                  <p><img alt="bao-hanh-san-pham" src="{{url('images/bao-hanh-san-pham.jpg')}}" style="display:block; margin-left:auto; margin-right:auto; width:60%" title="Phòng hỗ trợ và chăm sóc khách hàng tại FAST" /></p>
                  <p>Sau thời hạn bảo h&agrave;nh miễn ph&iacute; 12 th&aacute;ng, FAST v&agrave; kh&aacute;ch h&agrave;ng c&ugrave;ng nhau thỏa thuận v&agrave; k&yacute; kết c&aacute;c hợp đồng bảo tr&igrave; sản phẩm. C&aacute;c nh&acirc;n vi&ecirc;n của FAST lu&ocirc;n sẵn s&agrave;ng hỗ trợ v&agrave; giải quyết mọi kh&oacute; khăn của kh&aacute;ch h&agrave;ng qua điện thoại, qua thư điện tử, fax hoặc trực tiếp tại trụ sở của kh&aacute;ch h&agrave;ng.</p>
                  <p>FAST c&oacute; ri&ecirc;ng hệ thống th&ocirc;ng tin Fast CRM lưu trữ c&aacute;c vấn đề vướng mắc thường gặp v&agrave; c&aacute;ch thức giải quyết nhằm hỗ trợ cho kh&aacute;ch h&agrave;ng một c&aacute;ch nhanh nhất.</p>
                  <p>Dịch vụ hỗ trợ bảo h&agrave;nh, bảo tr&igrave; bao gồm c&aacute;c c&ocirc;ng việc sau:</p>
                  <h5><span style="color:#008080">1. Bảo h&agrave;nh sản phẩm</span></h5>
                  <p>C&ocirc;ng t&aacute;c bảo h&agrave;nh sản phẩm được thực hiện khi sản phẩm phần mềm đ&atilde; cung cấp cho kh&aacute;ch h&agrave;ng c&oacute; vấn đề ph&aacute;t sinh g&acirc;y ảnh hưởng đến qu&aacute; tr&igrave;nh hoạt động của doanh nghiệp.</p>
                  <p>Qu&aacute; tr&igrave;nh bảo h&agrave;nh c&oacute; thể được thực hiện qua điện thoại, internet, qua email, phần mềm truy cập từ xa hoặc trực tiếp tại kh&aacute;ch h&agrave;ng.</p>
                  <p>Th&ocirc;ng thường c&aacute;c y&ecirc;u cầu bảo h&agrave;nh của kh&aacute;ch h&agrave;ng được thực trong ng&agrave;y.</p>
                  <h5><span style="color:#008080">2. Bảo tr&igrave; định kỳ</span></h5>
                  <p>Định kỳ theo thời gian &ndash; h&agrave;ng qu&yacute; hoặc h&agrave;ng th&aacute;ng, nh&acirc;n vi&ecirc;n thuộc bộ phận chăm s&oacute;c kh&aacute;ch h&agrave;ng của FAST sẽ bảo tr&igrave; định kỳ tại trụ sở của kh&aacute;ch h&agrave;ng qua h&igrave;nh thức thăm hỏi t&igrave;nh h&igrave;nh sử dụng, ghi nhận c&aacute;c &yacute; kiến phản hồi, đề xuất của kh&aacute;ch h&agrave;ng, kiểm tra v&agrave; xử l&yacute; bảo tr&igrave; phần mềm nếu cần thiết.</p>
                  <h5><span style="color:#008080">3. Hỗ trợ trong qu&aacute; tr&igrave;nh sử dụng</span></h5>
                  <p>Hỗ trợ sử dụng được thực hiện khi c&oacute; c&aacute;c vấn đề ph&aacute;t sinh ảnh hưởng đến qu&aacute; tr&igrave;nh sử dụng sản phẩm v&agrave; hoạt động của doanh nghiệp.</p>
                  <p>Hỗ trợ sử dụng bao gồm c&aacute;c c&ocirc;ng việc như:</p>
                  <ul>
                     <li>Tư vấn sử dụng</li>
                     <li>Tư vấn khắc phục sự cố</li>
                     <li>Tư vấn qu&eacute;t virus</li>
                     <li>Tư vấn hỗ trợ sử dụng tr&ecirc;n phần mềm khi c&oacute; nghiệp vụ mới ph&aacute;t sinh tại doanh nghiệp&hellip;</li>
                  </ul>
                  <p>Hiện tại hệ thống bảo h&agrave;nh, bảo tr&igrave; v&agrave; hỗ trợ sử dụng của FAST đang phục vụ tr&ecirc;n <strong>15.000</strong> kh&aacute;ch h&agrave;ng tr&ecirc;n to&agrave;n quốc.</p>
               </div>
               
            </div>
            <div class="article-comment">
               <style>
                  #comments .media-list.show-all-parent .media.hidden:not(.media2) {
                  display : block !important;
                  }
                  #comments .media-list .group.show-all .media.hidden {
                  display : block !important;
                  }
               </style>
               <div class="facebook-comment">
                  <h4 class="article-tt-other">Gửi b&igrave;nh luận</h4>
                  <form method="POST" id="form-comment" accept-charset="UTF-8" action="/global-actions/create-comment/post/172">
                     <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="form-group">
                              <input type="text" name="name" class="form-control" value=""
                                 placeholder="Họ v&agrave; t&ecirc;n *" required autocomplete="off">
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <input type="text" name="email" class="form-control" value=""
                                 placeholder="Email *" required autocomplete="off">
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <input type="text" name="phone" class="form-control" value=""
                                 placeholder="Số điện thoại *" required autocomplete="off">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 text-right">
                           <button type="submit" class="btn btn-primary">Gửi b&igrave;nh luận</button>
                        </div>
                     </div>
                  </form>
                  <h4 class="article-tt-other">1 B&igrave;nh luận</h4>
                  <div class="row">
                     <div class="col-md-12">
                        <div id="comments">
                           <div class="media-list">
                              <script>
                                 function voteComment(id, type) {
                                     var el = $('[data-vote-comment=' + id + ']');
                                     var num = el.find('.vote-comment-txt').attr('vote-comment-count');
                                     num = +num + 1;
                                     if (type === 'down') {
                                         $data = {type: 'dislike'};
                                 
                                         el.find('.hasTip-down').text('(' + num + ')');
                                         el.find('.votedown-btn').addClass('active');
                                     } else {
                                         $data = {type: 'like'};
                                         el.find('.hasTip-up').text('(' + num + ')');
                                         el.find('.voteup-btn').addClass('active');
                                     }
                                     $.ajax({
                                         url: '/global-actions/rating-comment/' + id,
                                         type: 'POST',
                                         data: $data,
                                         cache: false,
                                         complete: function (data) {
                                             var _data = data.responseJSON;
                                             if (!_data.error) {
                                                 var like_count = _data.data.like_count,
                                                         dislike_count = _data.data.dislike_count;
                                 
                                                 el.find('.hasTip-up').text('(' + like_count + ')');
                                                 el.find('.hasTip-down').text('(' + dislike_count + ')');
                                 
                                                 if (type === 'down') {
                                                     el.find('.votedown-btn').addClass('active');
                                                 } else {
                                                     el.find('.voteup-btn').addClass('active');
                                                 }
                                 
                                                 el.find('.hasTip').removeAttr('href');
                                             }
                                         }
                                     });
                                 }
                                 $(document).ready(function () {
                                     $('body').on('click', '.fbs-vote-comment > .media-heading-toggle', function (event) {
                                         var $current = $(this);
                                         var $target = $($current.attr('href'));
                                 
                                         $target.find('textarea').html('@' + $current.attr('data-name') + ': ');
                                     });
                                 });
                              </script>
                              <div class="media  ">
                                 <div class="media-left">
                                    <a href="#">
                                    <img src="{{url('images/ad1.png')}}" alt="" class="media-object">
                                    </a>
                                 </div>
                                 <div class="media-body">
                                    <h4 class="media-heading">
                                       Huyen Nguyen
                                       <span>2017-07-07 12:26:43</span>
                                       <span data-vote-comment="828" class="fbs-vote-comment">
                                       <a role="button" data-toggle="collapse" href="#comments-828" aria-expanded="false"
                                          aria-controls="comments-828" class="media-heading-toggle"
                                          data-name="Huyen Nguyen">open</a>
                                       <a href="javascript:voteComment(828, 'up')" title="Th&iacute;ch"
                                          class="voteup-btn hasTip">
                                       <i vote-comment-count="0"
                                          class="vote-comment-txt hasTip-up">(0)</i>
                                       </a>
                                       <a href="javascript:voteComment(828, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                          class="votedown-btn hasTip">
                                       <i vote-comment-count="0"
                                          class="vote-comment-txt hasTip-down">(0)</i>
                                       </a>
                                       </span>
                                    </h4>
                                    <div class="clearfix">Cho mình hỏi phân hệ kho cuối kỳ có tồn kho theo ngày nhập hàng theo từng mặt hàng không vậy Fast.</div>
                                 </div>
                              </div>
                              <div class="group">
                                 <div class="media media2 ">
                                    <div class="media-left">
                                       <a href="#">
                                       <img src="{{url('images/ad1.png')}}" alt="" class="media-object">
                                       </a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading">
                                          C&Ocirc;NG TY FAST
                                          <span>2017-07-12 08:44:31</span>
                                          <span data-vote-comment="843" class="fbs-vote-comment">
                                          <a href="javascript:voteComment(843, 'up')" title="Th&iacute;ch"
                                             class="voteup-btn hasTip">
                                          <i vote-comment-count="0"
                                             class="vote-comment-txt hasTip-up">(0)</i>
                                          </a>
                                          <a href="javascript:voteComment(843, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                             class="votedown-btn hasTip">
                                          <i vote-comment-count="0"
                                             class="vote-comment-txt hasTip-down">(0)</i>
                                          </a>
                                          </span>
                                       </h4>
                                       <div class="clearfix">@Huyen Nguyen: Chào chị Huyền,<br />
                                          <br />
                                          Về vấn đề này, chị vui lòng liên hệ phòng CSKH tại chi nhánh gần nhất để được hỗ trợ chi tiết hơn ạ.<br />
                                          <br />
                                          Văn phòng tại TP Hà Nội<br />
                                          Ðiện thoại: (024) 3771-5590<br />
                                          Hỗ trợ ngoài giờ: 098-119-5590<br />
                                          Hỗ trợ ngoài giờ FAO: 097-297-3999<br />
                                          <br />
                                          Văn phòng tại TP HCM<br />
                                          Ðiện thoại: (028) 7108-8788<br />
                                          Hỗ trợ ngoài giờ: 1900 56 15 56<br />
                                          <br />
                                          Văn phòng tại TP Đà Nẵng<br />
                                          Ðiện thoại: (0236) 381-0532<br />
                                          Hỗ trợ ngoài giờ: 090-588-8462<br />
                                          Trân trọng,
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="collapse" id="comments-828">
                                 <form method="POST" id="form-comment" accept-charset="UTF-8"
                                    action="/global-actions/create-comment/post/172">
                                    <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                    <input type="hidden" name="parent_id" value="828">
                                    <div class="media media2">
                                       <div class="form-group">
                                          <input type="text" name="name" class="form-control" value=""
                                             placeholder="Họ v&agrave; t&ecirc;n *" required
                                             autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <input type="text" name="email" class="form-control" value=""
                                             placeholder="Email *" required autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <input type="text" name="phone" class="form-control" value=""
                                             placeholder="Số điện thoại *" required autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <textarea class="form-control" rows="5" name="content"
                                             placeholder="Nội dung"></textarea>
                                       </div>
                                       <div class="text-right">
                                          <button type="submit"
                                             class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</main>
@endsection