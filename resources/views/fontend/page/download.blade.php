@extends('fontend.layouts.index')
@section('content')
	<main class="main">
               <div class="container">
                  <div class="container">
                     <div class="main-left">
                        <nav class="menu-left aside-left">
                              <h3 class="title-left">MBSOFT Business Online</h3>
                              <ul>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/tinhnangchung') }}"
                                       title="T&iacute;nh năng chung">Tính năng chung</a>
                                 </li>
                                 
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/dangkydungthu') }}"
                                       title="Đăng k&yacute; d&ugrave;ng thử">Đăng k&yacute; d&ugrave;ng thử</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/banggiachung') }}"
                                       title="Bảng gi&aacute;">Bảng gi&aacute;</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/khachhang') }}"
                                       title="Kh&aacute;ch h&agrave;ng">Kh&aacute;ch h&agrave;ng</a>
                                 </li>
                                 <li>
                                    <a class="active"
                                       href="{{ url('/home/download') }}"
                                       title="Download t&agrave;i liệu">Download t&agrave;i liệu</a>
                                 </li>
                                 
                              </ul>
                           </nav>
                        <div class="related-products aside-left">
                              <h3 class="title-left">﻿Sản phẩm c&ugrave;ng nh&oacute;m</h3>
                              <ul>
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online"><img src="{{ url('images/DMS.jpg') }}"
                                          alt="MBSOFT DMS Online" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online">MBSOFT DMS Online</a></h4>
                                    </div>
                                 </li>
                                 
                                 
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial"><img src="{{ url('images/sp3.jpg')}}"
                                          alt="MBSOFT Financial" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial">MBSOFT Financial</a></h4>
                                    </div>
                                 </li>
                                
                              </ul>
                           </div>
                     </div>
                     <div class="main-right">
                        <div class="content-detail">
                           <h1 class="title-detail"> Tải c&aacute;c t&agrave;i liệu phần mềm MBSOFT Business Online</h1>
                           <div class="detail-top">
                              Ng&agrave;y đăng: <span class="color">2016-07-04 15:52:26</span> - Ng&agrave;y cập nhật: <span class="color">2017-08-23 10:59:08</span>
                              - Số lần xem: <span class="color">3513</span>
                           </div>
                           <div class="detail-rating">
                              <div class="rating-wrap ml0">
                                 <script>
                                    $(document).ready(function () {
                                        var text_rate = $('.rating-text-txt').html();
                                        var rated = false;
                                        $('input[name="rating"]').on('click', function (e) {
                                            e.preventDefault();
                                            var form_data = $(this).val();
                                    
                                            if (!rated) {
                                                $('.rating-text-txt')
                                                    .hide()
                                                    .addClass('rating-text-loading')
                                                    .html('Đang cập nhật...')
                                                    .fadeIn();
                                    
                                                $.ajax({
                                                    url: '/global-actions/rating/post/41',
                                                    type: 'POST',
                                                    data: {
                                                        value: form_data
                                                    },
                                                    cache: false,
                                                    complete: function (data) {
                                                        var _data = data.responseJSON;
                                                        var total = _data.data.total,
                                                            ratings_average = parseFloat(_data.data.ratings_average).toFixed(1);
                                                        if (!_data.error) {
                                                            rated = true;
                                                            $('.rating-text-txt')
                                                                .hide()
                                                                .removeClass('rating-text-loading')
                                                                .text('Cám ơn bạn đã cho điểm!')
                                                                .fadeIn();
                                                            setTimeout(function () {
                                                                text_rate = 'Điểm: <span class="color">' + ratings_average + '/5</span> (' + total + ' phiếu)';
                                                                $('.rating-text-txt')
                                                                    .hide()
                                                                    .html(text_rate)
                                                                    .fadeIn();
                                                            }, 2000);
                                                        }
                                                    }
                                                });
                                            } else {
                                                setTimeout(function () {
                                                    $('.rating-text-txt')
                                                        .hide()
                                                        .removeClass('rating-text-loading')
                                                        .html('Bạn đã cho điểm bài này rồi!')
                                                        .fadeIn();
                                                }, 1000);
                                                setTimeout(function () {
                                                    $('.rating-text-txt')
                                                        .hide()
                                                        .html(text_rate)
                                                        .fadeIn();
                                                }, 3000);
                                            }
                                        });
                                    });
                                 </script>
                                 <div class="rating-text">
                                    <span class="rating-text-txt">Điểm: <span class="color">4.5/5</span> (6 phiếu)</span>
                                 </div>
                                 <span class="rating">
                                 <input id="rating5" type="radio" name="rating" value="5" checked>
                                 <label for="rating5">5</label>
                                 <input id="rating4" type="radio" name="rating" value="4" >
                                 <label for="rating4">4</label>
                                 <input id="rating3" type="radio" name="rating" value="3" >
                                 <label for="rating3">3</label>
                                 <input id="rating2" type="radio" name="rating" value="2" >
                                 <label for="rating2">2</label>
                                 <input id="rating1" type="radio" name="rating" value="1" >
                                 <label for="rating1">1</label>
                                 </span>
                              </div>
                           </div>
                           <div class="social-bxcount">
                              <ul class="social">
                                 <li>
                                    Chia sẻ
                                 </li>
                                 <li>
                                    <div class="facebook" style="display: inline-block;">
                                       <div class="fb-like"
                                          data-href="#"
                                          data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                                       <div class="fb-share-button" data-href="#" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                                          <a class="fb-xfbml-parse-ignore" target="_blank" href="#&src=sdkpreparse">Share</a>
                                       </div>
                                       <div id="fb-root"></div>
                                       <script>(function(d, s, id) {
                                          var js, fjs = d.getElementsByTagName(s)[0];
                                          if (d.getElementById(id)) return;
                                          js = d.createElement(s); js.id = id;
                                          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
                                          fjs.parentNode.insertBefore(js, fjs);
                                          }(document, 'script', 'facebook-jssdk'));
                                       </script>
                                    </div>
                                 </li>
                                 <li>
                                    <!-- Place this tag in your head or just before your close body tag. -->
                                    <script src="https://apis.google.com/js/platform.js" async defer></script>
                                    <!-- Place this tag where you want the share button to render. -->
                                    <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                                 </li>
                              </ul>
                           </div>
                           <div class="article-content">
                              <p>&nbsp;</p>
                              <h2><strong><span style="font-size:14px">Tải về t&agrave;i liệu v&agrave; mẫu b&aacute;o c&aacute;o in ra từ phần mềm ERP MBSOFT Business Online</span></strong></h2>
                              <p><u><strong><span style="color:#008080">[&nbsp;</span><a href="#" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Việt"><span style="color:#008080">Tải về t&agrave;i liệu giới thiệu MBSOFT Business Online tiếng Việt</span></a><span style="color:#008080">&nbsp;]</span></strong></u></p>
                              <p><u><strong><span style="color:#008080">[&nbsp;</span><a href="http://dl2.MBSOFT.com.vn/FMK/2017.07.EN.FB-Profile-DS.pdf" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Anh"><span style="color:#008080">Tải về t&agrave;i liệu giới thiệu MBSOFT Business </span></a><a href="#" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Việt"><span style="color:#008080">Online</span></a><a href="http://dl2.MBSOFT.com.vn/FMK/2017.07.EN.FB-Profile-DS.pdf" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Anh"><span style="color:#008080">&nbsp;tiếng Anh</span></a><span style="color:#008080">&nbsp;]</span></strong></u></p>
                              <p><u><strong><span style="color:#008080">[&nbsp;</span><a href="http://dl2.MBSOFT.com.vn/MBSOFT-business/docs/MBSOFT-business-3.1-mot-so-mau-bao-cao.rar" target="_blank" title="Tải về mẫu báo cáo in ra từ phần mềm MBSOFT Business"><span style="color:#008080">Tải về mẫu b&aacute;o c&aacute;o in ra từ phần mềm MBSOFT Business</span></a><span style="color:#008080">&nbsp;</span><a href="#" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Việt"><span style="color:#008080">Online</span></a><span style="color:#008080">]</span></strong></u></p>
                              <p><u><strong><span style="color:#008080">[&nbsp;</span><a href="http://diendan.MBSOFT.com.vn/downloads.php?do=file&amp;id=56" target="_blank" title="Tải về tài liệu hướng dẫn sử dụng MBSOFT Business tiếng Việt"><span style="color:#008080">Tải về t&agrave;i liệu hướng dẫn sử dụng MBSOFT Business </span></a><a href="#" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Việt"><span style="color:#008080">Online</span></a><a href="http://diendan.MBSOFT.com.vn/downloads.php?do=file&amp;id=56" target="_blank" title="Tải về tài liệu hướng dẫn sử dụng MBSOFT Business tiếng Việt"><span style="color:#008080">&nbsp;tiếng Việt</span></a><span style="color:#008080">&nbsp;]</span></strong></u></p>
                              <p><u><strong><span style="color:#008080">[&nbsp;</span><a href="http://diendan.MBSOFT.com.vn/downloads.php?do=file&amp;id=57" target="_blank" title="Tải về tài liệu hướng dẫn sử dụng MBSOFT Business tiếng Anh"><span style="color:#008080">Tải về t&agrave;i liệu hướng dẫn sử dụng MBSOFT Business </span></a><a href="#" target="_blank" title="Tải về tài liệu giới thiệu MBSOFT Business tiếng Việt"><span style="color:#008080">Online</span></a><a href="http://diendan.MBSOFT.com.vn/downloads.php?do=file&amp;id=57" target="_blank" title="Tải về tài liệu hướng dẫn sử dụng MBSOFT Business tiếng Anh"><span style="color:#008080">&nbsp;tiếng Anh</span></a><span style="color:#008080">&nbsp;]</span></strong></u></p>
                              <p>&nbsp;</p>
                           </div>
                           <div class="tags-wrap">
                              <span>Tags:</span>
                              <a href="/tags?k=Download+t&agrave;i+liệu" title="Download t&agrave;i liệu"
                                 style="color: #00b6bc;">Download t&agrave;i liệu</a>,
                              <a href="/tags?k=+Tải+t&agrave;i+liệu" title=" Tải t&agrave;i liệu"
                                 style="color: #00b6bc;"> Tải t&agrave;i liệu</a>,
                           </div>
                        </div>
                        <div class="article-comment">
                           <style>
                              #comments .media-list.show-all-parent .media.hidden:not(.media2) {
                              display : block !important;
                              }
                              #comments .media-list .group.show-all .media.hidden {
                              display : block !important;
                              }
                           </style>
                           <div class="facebook-comment">
                              <h4 class="article-tt-other">Gửi b&igrave;nh luận</h4>
                              <form method="POST" id="form-comment" accept-charset="UTF-8" action="/global-actions/create-comment/post/41">
                                 <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                 <div class="row">
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <input type="text" name="name" class="form-control" value=""
                                             placeholder="Họ v&agrave; t&ecirc;n *" required autocomplete="off">
                                       </div>
                                    </div>
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <input type="text" name="email" class="form-control" value=""
                                             placeholder="Email *" required autocomplete="off">
                                       </div>
                                    </div>
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <input type="text" name="phone" class="form-control" value=""
                                             placeholder="Số điện thoại *" required autocomplete="off">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12 text-right">
                                       <button type="submit" class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                    </div>
                                 </div>
                              </form>
                              <h4 class="article-tt-other">0 B&igrave;nh luận</h4>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div id="comments">
                                       <div class="media-list">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
            </main>
@endsection