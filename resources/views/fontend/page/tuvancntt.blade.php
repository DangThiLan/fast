@extends('fontend.layouts.index')
@section('content')
<main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">﻿Dịch vụ</h3>
                              <ul>
                                 <li class="">
                                    <a title="Tư vấn ứng dụng"
                                       href="{{ url('home/tuvanungdung') }}">Tư vấn ứng dụng</a>
                                 </li>
                                 <li class="">
                                    <a title="Tư vấn, hỗ trợ sử dụng"
                                       href="{{ url('home/tuvanhotro') }}">Tư vấn, hỗ trợ sử dụng</a>
                                 </li>
                                 <li class="active">
                                    <a title="Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin"
                                       href="{{ url('home/tuvancntt') }}">Dịch vụ c&ocirc;ng nghệ th&ocirc;ng tin</a>
                                 </li>
                              </ul>
                           </nav>
                        </div>
                        <div class="main-right">
                           <div class="content-detail">
                              <h1 class="title-detail"> Dịch vụ C&ocirc;ng nghệ Th&ocirc;ng tin</h1>
                              <div class="detail-top">
                                 Ng&agrave;y đăng: <span class="color">2016-08-02 21:24:10</span> - Ng&agrave;y cập nhật: <span class="color">2016-11-21 11:25:09</span>
                                 - Số lần xem: <span class="color">202</span>
                              </div>
                              <div class="detail-rating">
                                 <div class="rating-wrap ml0">
                                    <script>
                                       $(document).ready(function () {
                                           var text_rate = $('.rating-text-txt').html();
                                           var rated = false;
                                           $('input[name="rating"]').on('click', function (e) {
                                               e.preventDefault();
                                               var form_data = $(this).val();
                                       
                                               if (!rated) {
                                                   $('.rating-text-txt')
                                                       .hide()
                                                       .addClass('rating-text-loading')
                                                       .html('Đang cập nhật...')
                                                       .fadeIn();
                                       
                                                   $.ajax({
                                                       url: '/global-actions/rating/post/189',
                                                       type: 'POST',
                                                       data: {
                                                           value: form_data
                                                       },
                                                       cache: false,
                                                       complete: function (data) {
                                                           var _data = data.responseJSON;
                                                           var total = _data.data.total,
                                                               ratings_average = parseFloat(_data.data.ratings_average).toFixed(1);
                                                           if (!_data.error) {
                                                               rated = true;
                                                               $('.rating-text-txt')
                                                                   .hide()
                                                                   .removeClass('rating-text-loading')
                                                                   .text('Cám ơn bạn đã cho điểm!')
                                                                   .fadeIn();
                                                               setTimeout(function () {
                                                                   text_rate = 'Điểm: <span class="color">' + ratings_average + '/5</span> (' + total + ' phiếu)';
                                                                   $('.rating-text-txt')
                                                                       .hide()
                                                                       .html(text_rate)
                                                                       .fadeIn();
                                                               }, 2000);
                                                           }
                                                       }
                                                   });
                                               } else {
                                                   setTimeout(function () {
                                                       $('.rating-text-txt')
                                                           .hide()
                                                           .removeClass('rating-text-loading')
                                                           .html('Bạn đã cho điểm bài này rồi!')
                                                           .fadeIn();
                                                   }, 1000);
                                                   setTimeout(function () {
                                                       $('.rating-text-txt')
                                                           .hide()
                                                           .html(text_rate)
                                                           .fadeIn();
                                                   }, 3000);
                                               }
                                           });
                                       });
                                    </script>
                                    <div class="rating-text">
                                       <span class="rating-text-txt">Điểm: <span class="color">5/5</span> (1 phiếu)</span>
                                    </div>
                                    <span class="rating">
                                    <input id="rating5" type="radio" name="rating" value="5" checked>
                                    <label for="rating5">5</label>
                                    <input id="rating4" type="radio" name="rating" value="4" >
                                    <label for="rating4">4</label>
                                    <input id="rating3" type="radio" name="rating" value="3" >
                                    <label for="rating3">3</label>
                                    <input id="rating2" type="radio" name="rating" value="2" >
                                    <label for="rating2">2</label>
                                    <input id="rating1" type="radio" name="rating" value="1" >
                                    <label for="rating1">1</label>
                                    </span>
                                 </div>
                              </div>
                              <div class="social-bxcount">
                                 <ul class="social">
                                    <li>
                                       Chia sẻ
                                    </li>
                                    <li>
                                       <div class="facebook" style="display: inline-block;">
                                          <div class="fb-like"
                                             data-href="#"
                                             data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                                          <div class="fb-share-button" data-href="#" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                                             <a class="fb-xfbml-parse-ignore" target="_blank" href="#&src=sdkpreparse">Share</a>
                                          </div>
                                          <div id="fb-root"></div>
                                          <script>(function(d, s, id) {
                                             var js, fjs = d.getElementsByTagName(s)[0];
                                             if (d.getElementById(id)) return;
                                             js = d.createElement(s); js.id = id;
                                             js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
                                             fjs.parentNode.insertBefore(js, fjs);
                                             }(document, 'script', 'facebook-jssdk'));
                                          </script>
                                       </div>
                                    </li>
                                    <li>
                                       <!-- Place this tag in your head or just before your close body tag. -->
                                       <script src="https://apis.google.com/js/platform.js" async defer></script>
                                       <!-- Place this tag where you want the share button to render. -->
                                       <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div class="article-content">
                                 <p><img alt="Dịch vụ bảo trì hệ thống máy tính, mạng" src="{{url('images/mang-may-tinh.jpg')}}" style="display:block; height:137px; margin-left:auto; margin-right:auto; width:400px" title="Dịch vụ bảo trì hệ thống máy tính, mạng" /></p>
                                 <p>FAST cung cấp c&aacute;c sản phẩm v&agrave; dịch vụ CNTT sau:</p>
                                 <ol>
                                    <li>Bảo tr&igrave; hệ thống m&aacute;y t&iacute;nh, mạng, m&aacute;y chủ cho kh&aacute;ch h&agrave;ng.</li>
                                    <li>Cung cấp m&aacute;y chủ ch&iacute;nh h&atilde;ng IBM-Lenovo, Dell, HP&hellip;</li>
                                    <li>Cung cấp c&aacute;c phần mềm c&oacute; bản quyền của Microsoft cũng như c&aacute;c h&atilde;ng kh&aacute;c</li>
                                    <li>Cung cấp phần mềm diệt virus Kaspersky</li>
                                    <li>Cung cấp m&aacute;y t&iacute;nh, linh kiện tin học</li>
                                    <li>Thiết kế, thi c&ocirc;ng hệ thống mạng, thoại</li>
                                    <li>Lắp đặt hệ thống camera</li>
                                    <li>Cung cấp m&aacute;y in, sửa m&aacute;y in, đổ mực in</li>
                                    <li>Cung cấp email, quản trị hệ thống email</li>
                                    <li>Thiết kế, bảo tr&igrave; website cho kh&aacute;ch h&agrave;ng</li>
                                    <li>Cho thu&ecirc; m&aacute;y chủ vật l&yacute;, m&aacute;y chủ ảo</li>
                                    <li>&nbsp;&hellip;</li>
                                 </ol>
                                 <p>Như vậy kết hợp sử dụng sản phẩm v&agrave; dịch vụ CNTT với sản phẩm v&agrave; dịch vụ phần mềm th&igrave; kh&aacute;ch h&agrave;ng c&oacute; thể sử dụng dịch vụ trọn g&oacute;i của FAST, kh&aacute;ch h&agrave;ng chỉ cần l&agrave;m việc với một đầu mối duy nhất, r&uacute;t ngắn thời gian, giảm chi ph&iacute; giao dịch, tăng hiệu quả đối với quyết định đầu tư.</p>
                                 <p>Mọi th&ocirc;ng tin chi tiết xin li&ecirc;n hệ trực tiếp hoặc chat để được tư vấn, hỗ trợ tốt hơn.</p>
                              </div>
                              
                           </div>
                           <div class="article-comment">
                              <style>
                                 #comments .media-list.show-all-parent .media.hidden:not(.media2) {
                                 display : block !important;
                                 }
                                 #comments .media-list .group.show-all .media.hidden {
                                 display : block !important;
                                 }
                              </style>
                              <div class="facebook-comment">
                                 <h4 class="article-tt-other">Gửi b&igrave;nh luận</h4>
                                 <form method="POST" id="form-comment" accept-charset="UTF-8" action="/global-actions/create-comment/post/189">
                                    <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                    <div class="row">
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <input type="text" name="name" class="form-control" value=""
                                                placeholder="Họ v&agrave; t&ecirc;n *" required autocomplete="off">
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <input type="text" name="email" class="form-control" value=""
                                                placeholder="Email *" required autocomplete="off">
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <input type="text" name="phone" class="form-control" value=""
                                                placeholder="Số điện thoại *" required autocomplete="off">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                       </div>
                                    </div>
                                 </form>
                                 <h4 class="article-tt-other">0 B&igrave;nh luận</h4>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div id="comments">
                                          <div class="media-list">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection