@extends('fontend.layouts.index')
@section('content')
	<main class="main">
               <div class="container">
                  <div class="container">
                     <div class="main-left">
                        <nav class="menu-left aside-left">
                              <h3 class="title-left">Fast Business Online</h3>
                              <ul>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/tinhnangchung') }}"
                                       title="T&iacute;nh năng chung">Tính năng chung</a>
                                 </li>
                                 
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/dangkydungthu') }}"
                                       title="Đăng k&yacute; d&ugrave;ng thử">Đăng k&yacute; d&ugrave;ng thử</a>
                                 </li>
                                 <li>
                                    <a class="active"
                                       href="{{ url('/home/banggiachung') }}"
                                       title="Bảng gi&aacute;">Bảng gi&aacute;</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/khachhang') }}"
                                       title="Kh&aacute;ch h&agrave;ng">Kh&aacute;ch h&agrave;ng</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/download') }}"
                                       title="Download t&agrave;i liệu">Download t&agrave;i liệu</a>
                                 </li>
                                 
                              </ul>
                           </nav>
                        <div class="related-products aside-left">
                              <h3 class="title-left">﻿Sản phẩm c&ugrave;ng nh&oacute;m</h3>
                              <ul>
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online"><img src="{{ url('images/DMS.jpg') }}"
                                          alt="MBSOFT DMS Online" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online">MBSOFT DMS Online</a></h4>
                                    </div>
                                 </li>
                                 
                                 
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial"><img src="{{ url('images/sp3.jpg')}}"
                                          alt="MBSOFT Financial" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial">MBSOFT Financial</a></h4>
                                    </div>
                                 </li>
                                
                              </ul>
                           </div>
                     </div>
                     <div class="main-right">
                        <div class="content-detail">
                           <h1 class="title-detail"> Bảng gi&aacute; Phần mềm ERP Fast Business Online</h1>
                           <div class="detail-top">
                              Ng&agrave;y đăng: <span class="color">2016-06-28 06:46:23</span> - Ng&agrave;y cập nhật: <span class="color">2017-01-06 16:01:29</span>
                              - Số lần xem: <span class="color">9911</span>
                           </div>
                           <div class="detail-rating">
                              <div class="rating-wrap ml0">
                                 <script>
                                    $(document).ready(function () {
                                        var text_rate = $('.rating-text-txt').html();
                                        var rated = false;
                                        $('input[name="rating"]').on('click', function (e) {
                                            e.preventDefault();
                                            var form_data = $(this).val();
                                    
                                            if (!rated) {
                                                $('.rating-text-txt')
                                                    .hide()
                                                    .addClass('rating-text-loading')
                                                    .html('Đang cập nhật...')
                                                    .fadeIn();
                                    
                                                $.ajax({
                                                    url: '/global-actions/rating/post/23',
                                                    type: 'POST',
                                                    data: {
                                                        value: form_data
                                                    },
                                                    cache: false,
                                                    complete: function (data) {
                                                        var _data = data.responseJSON;
                                                        var total = _data.data.total,
                                                            ratings_average = parseFloat(_data.data.ratings_average).toFixed(1);
                                                        if (!_data.error) {
                                                            rated = true;
                                                            $('.rating-text-txt')
                                                                .hide()
                                                                .removeClass('rating-text-loading')
                                                                .text('Cám ơn bạn đã cho điểm!')
                                                                .fadeIn();
                                                            setTimeout(function () {
                                                                text_rate = 'Điểm: <span class="color">' + ratings_average + '/5</span> (' + total + ' phiếu)';
                                                                $('.rating-text-txt')
                                                                    .hide()
                                                                    .html(text_rate)
                                                                    .fadeIn();
                                                            }, 2000);
                                                        }
                                                    }
                                                });
                                            } else {
                                                setTimeout(function () {
                                                    $('.rating-text-txt')
                                                        .hide()
                                                        .removeClass('rating-text-loading')
                                                        .html('Bạn đã cho điểm bài này rồi!')
                                                        .fadeIn();
                                                }, 1000);
                                                setTimeout(function () {
                                                    $('.rating-text-txt')
                                                        .hide()
                                                        .html(text_rate)
                                                        .fadeIn();
                                                }, 3000);
                                            }
                                        });
                                    });
                                 </script>
                                 <div class="rating-text">
                                    <span class="rating-text-txt">Điểm: <span class="color">5/5</span> (1 phiếu)</span>
                                 </div>
                                 <span class="rating">
                                 <input id="rating5" type="radio" name="rating" value="5" checked>
                                 <label for="rating5">5</label>
                                 <input id="rating4" type="radio" name="rating" value="4" >
                                 <label for="rating4">4</label>
                                 <input id="rating3" type="radio" name="rating" value="3" >
                                 <label for="rating3">3</label>
                                 <input id="rating2" type="radio" name="rating" value="2" >
                                 <label for="rating2">2</label>
                                 <input id="rating1" type="radio" name="rating" value="1" >
                                 <label for="rating1">1</label>
                                 </span>
                              </div>
                           </div>
                           <div class="social-bxcount">
                              <ul class="social">
                                 <li>
                                    Chia sẻ
                                 </li>
                                 <li>
                                    <div class="facebook" style="display: inline-block;">
                                       <div class="fb-like"
                                          data-href="#"
                                          data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                                       <div class="fb-share-button" data-href="#" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                                          <a class="fb-xfbml-parse-ignore" target="_blank" href="#">Share</a>
                                       </div>
                                       <div id="fb-root"></div>
                                       <script>(function(d, s, id) {
                                          var js, fjs = d.getElementsByTagName(s)[0];
                                          if (d.getElementById(id)) return;
                                          js = d.createElement(s); js.id = id;
                                          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
                                          fjs.parentNode.insertBefore(js, fjs);
                                          }(document, 'script', 'facebook-jssdk'));
                                       </script>
                                    </div>
                                 </li>
                                 <li>
                                    <!-- Place this tag in your head or just before your close body tag. -->
                                    <script src="https://apis.google.com/js/platform.js" async defer></script>
                                    <!-- Place this tag where you want the share button to render. -->
                                    <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                                 </li>
                              </ul>
                           </div>
                           <article class="article-content">
                              <style scoped>
                                 a {
                                 color : #00b6bc;
                                 }
                              </style>
                            
                              <div class="content-before">
                                 <p><strong>Phần mềm ERP</strong> <strong><a href="{{url('home/banggiasp1') }}}" target="_blank">Fast Business Online</a></strong> l&agrave; giải ph&aacute;p&nbsp;quản trị to&agrave;n diện doanh nghiệp được ph&aacute;t triển tr&ecirc;n nền tảng web. <em>Phần mềm ERP</em> cho ph&eacute;p truy cập l&agrave;m việc mọi nơi, mọi l&uacute;c từ c&aacute;c thiết bị c&oacute; kết nối được internet. &nbsp;</p>
                                 <p>Gi&aacute; của<strong> Phần mềm ERP</strong>&nbsp;Fast Business Online được t&iacute;nh theo c&aacute;c yếu tố sau:</p>
                                 <ol>
                                    <li>C&aacute;c ph&acirc;n hệ sử dụng</li>
                                    <li>Số lượng tối đa người sử dụng đồng thời (concurrent users)</li>
                                 </ol>
                                 <p><strong>Dưới đ&acirc;y l&agrave; bảng gi&aacute; tham khảo bản quyền <a href="{{url('home/banggiasp1') }}}" target="_blank">Fast Business Online</a>:</strong><br />
                                    <em>(Bảng gi&aacute; c&oacute; hiệu lực từ 1-4-2015 v&agrave; c&oacute; thể thay đổi theo ch&iacute;nh s&aacute;ch của C&ocirc;ng ty FAST)</em>
                                 </p>
                              </div>
                              <div class="table-responsive">
                                 <table class="table table-striped">
                                    <thead>
                                       <tr>
                                          <th width="60" class="text-center">STT</th>
                                          <th>Phân hệ</th>
                                          <th class="text-center">Giá</th>
                                          <th width="200" class="text-center">Đã bao gồm tối đa số lượng sử dụng đồng thời</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td class="text-center">1</td>
                                          <td>Quản trị hệ thống</td>
                                          <td class="text-center">1.000$</td>
                                          <td class="text-center">2</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">2</td>
                                          <td>Kế to&aacute;n</td>
                                          <td class="text-center">11.000$</td>
                                          <td class="text-center">10</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">3</td>
                                          <td>Quản l&yacute;  b&aacute;n h&agrave;ng</td>
                                          <td class="text-center">6.000$</td>
                                          <td class="text-center">5</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">4</td>
                                          <td>Quản l&yacute; mua h&agrave;ng  </td>
                                          <td class="text-center">5.000$</td>
                                          <td class="text-center">5</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">5</td>
                                          <td>Quản l&yacute; kho </td>
                                          <td class="text-center">4.000$</td>
                                          <td class="text-center">5</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">6</td>
                                          <td>Quản l&yacute; nh&acirc;n sự - chấm c&ocirc;ng - t&iacute;nh lương</td>
                                          <td class="text-center">8.000$</td>
                                          <td class="text-center">5</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">7</td>
                                          <td>Quản l&yacute; quan hệ kh&aacute;ch h&agrave;ng CRM </td>
                                          <td class="text-center">5.000$</td>
                                          <td class="text-center">5</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">8</td>
                                          <td>Ph&acirc;n t&iacute;ch số liệu (Fast Analytics)</td>
                                          <td class="text-center">2.000$</td>
                                          <td class="text-center">2</td>
                                       </tr>
                                       <tr>
                                          <td class="text-center">9</td>
                                          <td> Th&ecirc;m người sử dụng</td>
                                          <td class="text-center">200$/người  </td>
                                          <td class="text-center"></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                              <div class="content-after">
                                 <p><strong>Lưu &yacute;:</strong></p>
                                 <ul>
                                    <li>Bảng gi&aacute; tr&ecirc;n chỉ l&agrave; bảng gi&aacute; tham khảo.</li>
                                    <li>
                                       <p>Gi&aacute; tr&ecirc;n l&agrave; gi&aacute; bản quyền <strong>Phần mềm ERP</strong> bản chuẩn với 1 data v&agrave; 1 đơn vị cơ sở, chưa c&oacute; chỉnh sửa theo y&ecirc;u cầu v&agrave; chưa c&oacute; c&aacute;c dịch vụ tư vấn triển khai. C&aacute;c chi ph&iacute; chỉnh sửa phần mềm theo y&ecirc;u cầu, khảo s&aacute;t, tư vấn ứng dụng, c&agrave;i đặt v&agrave; đ&agrave;o tạo sẽ được t&iacute;nh ri&ecirc;ng v&agrave; b&aacute;o gi&aacute; cho từng kh&aacute;ch h&agrave;ng cụ thể.&nbsp;</p>
                                    </li>
                                    <li>Gi&aacute; cụ thể cho từng kh&aacute;ch h&agrave;ng t&ugrave;y thuộc b&agrave;i to&aacute;n thực tế đơn giản hay phức tạp theo ng&agrave;nh nghề, y&ecirc;u cầu quản trị, y&ecirc;u cầu lập tr&igrave;nh chỉnh sửa, chuyển đổi số liệu, chi ph&iacute; đi lại, lưu tr&uacute; đối với c&aacute;c đơn vị ở xa... v&agrave; sẽ được b&aacute;o gi&aacute; sau khi khảo s&aacute;t thực tế. Ngo&agrave;i ra c&ocirc;ng ty c&ograve;n c&oacute; ch&iacute;nh s&aacute;ch giảm gi&aacute; đối với c&aacute;c kh&aacute;ch h&agrave;ng cũ n&acirc;ng cấp l&ecirc;n sản phẩm mới.</li>
                                 </ul>
                                 <p>Để được tư vấn cụ thể kh&aacute;ch h&agrave;ng h&atilde;y li&ecirc;n hệ với c&aacute;c văn ph&ograve;ng của FAST.&nbsp;</p>
                                 <p>Để t&igrave;m hiểu về <strong>Phần mềm ERP</strong> Fast Business Online vui l&ograve;ng xem<span style="font-size:14px"><strong><span style="color:#000080"> </span><span style="color:#008080">[ </span><a href="{{url('home/banggiasp1') }}}" target="_blank"><span style="color:#008080">tại đ&acirc;y</span></a><span style="color:#008080"> ]</span></strong></span>.</p>
                                 <p><strong>Tr&acirc;n trọng,</strong></p>
                                 <p><strong>C&ocirc;ng ty FAST.</strong></p>
                              </div>
                              
                              <div class="tags-wrap">
                                 <span>Tags:</span>
                                 <a href="{{url('home/banggiachung') }}}" title="Bảng gi&aacute; phần mềm"
                                    style="color: #00b6bc;">Bảng gi&aacute; phần mềm</a>,
                              </div>
                           </article>
                        </div>
                        <div class="article-comment">
                           <style>
                              #comments .media-list.show-all-parent .media.hidden:not(.media2) {
                              display : block !important;
                              }
                              #comments .media-list .group.show-all .media.hidden {
                              display : block !important;
                              }
                           </style>
                           <div class="facebook-comment">
                              <h4 class="article-tt-other">Gửi b&igrave;nh luận</h4>
                              <form method="POST" id="form-comment" accept-charset="UTF-8" action="/global-actions/create-comment/post/23">
                                 <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                 <div class="row">
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <input type="text" name="name" class="form-control" value=""
                                             placeholder="Họ v&agrave; t&ecirc;n *" required autocomplete="off">
                                       </div>
                                    </div>
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <input type="text" name="email" class="form-control" value=""
                                             placeholder="Email *" required autocomplete="off">
                                       </div>
                                    </div>
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <input type="text" name="phone" class="form-control" value=""
                                             placeholder="Số điện thoại *" required autocomplete="off">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12 text-right">
                                       <button type="submit" class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                    </div>
                                 </div>
                              </form>
                              <h4 class="article-tt-other">0 B&igrave;nh luận</h4>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div id="comments">
                                       <div class="media-list">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
            </main>
@endsection
