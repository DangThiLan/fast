@extends('fontend.layouts.index')
@section('content')
   <main class="main">
                  <div class="container">
                     <nav class="nav-sol">
                        <div class="container">
                           <ul>
                              <li>
                                 <a class="active"
                                    href="#"
                                    title="Giải thưởng cho c&aacute;c sản phẩm">
                                 <span>Giải thưởng của chúng tôi</span>
                                 </a>
                              </li>
                              <li>
                                 <p class=""
                                    href="/giai-thuong-cho-cong-ty"
                                    title="Giải thưởng cho c&ocirc;ng ty">
                                 <span></span>
                                 </p>
                              </li>
                              <li>
                                 <p class=""
                                    href="/giai-thuong-ve-dich-vu-tu-van"
                                    title="Giải thưởng về dịch vụ tư vấn">
                                 <span></span>
                                 </p>
                              </li>
                           </ul>
                        </div>
                     </nav>
                     <div class="container">
                        <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">Giải thưởng cho c&aacute;c sản phẩm</h3>
                              <ul role="tablist">
                                 <li role="presentation" class="active">
                                    <a href="#post_996_1146" role="tab" data-toggle="tab"
                                       title="MBSOFT Business Online">MBSOFT Business Online</a>
                                 </li>
                                 <li role="presentation" class="">
                                    <a href="#post_662_686" role="tab" data-toggle="tab"
                                       title="MBSOFT Business">MBSOFT Business</a>
                                 </li>
                                 
                              </ul>
                           </nav>
                        </div>
                        <div class="main-right">
                           <div class="row row-award tab-content">
                              <div role="tabpanel" class="tab-pane active"
                                 id="post_996_1146">
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/2017.Sao-Khue.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2017 FBO được c&ocirc;ng nhận danh hiệu Sao Khu&ecirc; được trao bởi Hiệp hội Phần mềm Việt Nam - VINASA.
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/thumb1.png') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2015 đạt giải thưởng &quot;Nh&acirc;n t&agrave;i Đất Việt&quot; do Tập đo&agrave;n VNPT, Đ&agrave;i truyền h&igrave;nh Việt Nam v&agrave; B&aacute;o D&acirc;n tr&iacute; trao tặng. 
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/2014.FBO.ICT.260-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2014 đạt giải C&ocirc;ng nghệ th&ocirc;ng tin - Truyền th&ocirc;ng TPHCM 2014 (ICT Awards) do Sở CNTT -TT TPHCM trao tặng.
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/2014.Bangkhen-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2014 FBO đ&atilde; được bằng khen của Chủ tịch UBND Tp. HCM trong c&ocirc;ng t&aacute;c nghi&ecirc;n cứu, sản xuất giải ph&aacute;p phần mềm ti&ecirc;u biểu. 
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/thumb2.png') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2013 được trao giải thưởng Sao Khu&ecirc; của Hiệp hội Phần mềm Việt Nam - VINASA.
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane "
                                 id="post_662_686">
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/CupICT2011-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2012, MBSOFT Business được trao CUP CNTT-TT Tp.HCM v&agrave; bằng khen cho đơn vị c&oacute; sản phẩm v&agrave; giải ph&aacute;p phần mềm ti&ecirc;u biểu.
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/Bitcup2011 ERP-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2010, sản phẩm được trao giải &ldquo;BIT CUP&rdquo; &ndash; &ldquo;Giải ph&aacute;p CNTT hay nhất&rdquo; năm 2010 do bạn đọc Tạp ch&iacute; Thế giới Vi t&iacute;nh b&igrave;nh chọn.
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/thumb2.png') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2005 MBSOFT Business đ&atilde; được trao giải thưởng Sao Khu&ecirc; của Hiệp hội Phần mềm Việt Nam - VINASA.
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane "
                                 id="post_663_687">
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/thumb4.png') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2010 phần mềm quản trị t&agrave;i ch&iacute;nh kế to&aacute;n MBSOFT Financial đạt giải &ldquo;Sao Khu&ecirc; 2010&rdquo; do VINASA b&igrave;nh chọn. 
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane "
                                 id="post_997_1147">
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/2014.FA11.ICT.260-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2014 MBSOFT Accounting đạt giải CT Awards do Sở CNTT -TT TPHCM trao tặng. 
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/thumb2.png') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2014 MBSOFT Accounting được trao giải Sao Khu&ecirc; danh hiệu Phần mềm ti&ecirc;u biểu Hiệp hội phần mềm Việt Nam trao tặng. 
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/Bitcup2007-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2007 MBSOFT Accounting được trao giải &ldquo;Sản phẩm CNTT hay nhất&rdquo; &ndash; BIT CUP do độc giả Tạp ch&iacute; &ldquo;Thế giới Vi T&iacute;nh&rdquo; b&igrave;nh chọn.
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/2003.ICweek-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 2003, MBSOFT Accounting đoạt CUP CNTT-TT Việt Nam của Hội Tin học Việt Nam.
                                       </figcaption>
                                    </figure>
                                 </div>
                                 <div class="col-sm-4">
                                    <figure>
                                       <img src="{{ asset('images/1999.HCV-01.jpg') }}" alt="Giải thưởng cho c&aacute;c sản phẩm">
                                       <figcaption>
                                          Năm 1999 MBSOFT Accounting đoạt Huy chương v&agrave;ng &ldquo;Sản phẩm nhiều người d&ugrave;ng&rdquo; tại triễn l&atilde;m tin học quốc tế VCW Expo.
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection