@extends('fontend.layouts.index')
@section('content')
	 <main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="row row-contact-loca">
                           <div class="col-xs-12 col-md-6 col-lg-6">
                              <span class="ico-wrap">
                              <i class="fa fa-map-marker"></i>
                              </span>
                              <h3>Văn ph&ograve;ng tại TP H&agrave; Nội</h3>
                              <div>
                                 Tầng 3, Tòa nhà CT1B, Khu VOV, P.Mễ Trì, Q. Nam Từ Liêm, Hà Nội <br>
                                 <dl class="dl-horizontal dl-phonefax">
                                    <dt>Ðiện thoại :</dt>
                                    <dd><span class="bold">(024) 7108-8288</span> </dd>
                                    <dt>Fax :</dt>
                                    <dd>(024) 3771-5591</dd>
                                    </dd>
                                 </dl>
                                 Hỗ trợ ngoài giờ: 098-119-5590<br>
                                 Hỗ trợ ngoài giờ FAO: 097-297-3999
                              </div>
                           </div>
                           
                           
                           <div class="col-xs-12 col-md-6 col-lg-6">
                              <span class="ico-wrap">
                              <i class="fa fa-globe"></i>
                              </span>
                              <div>
                                 <dl>
                                    <dt>Email</dt>
                                    <dd>
                                       <p>info@mbsoft.com.vn</p>
                                    </dd>
                                    <dt>
                                       Website
                                    </dt>
                                    <dd>
                                       <p>www.mbsoft.com.vn</p>
                                    </dd>
                                 </dl>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="map-contact">
                        <div id="map_container"></div>
                        <div id="map"></div>
                        <div class="container">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7450.511431902847!2d105.80951877225762!3d20.982384261535845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acec14cdcfb9%3A0xb4f53c8207b23974!2zS2ltIEdpYW5nLCBUaGFuaCBYdcOibiwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1524111543219" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                     </div>
                     <div class="container">
                        <div class="row row-contact">
                           <div class="col-sm-6">
                              <h3 class="title-udc">Gửi th&ocirc;ng tin li&ecirc;n hệ</h3>
                              <!--div class="text-center"><p>Để biết th&ecirc;m th&ocirc;ng tin chi tiết hoặc chuyển y&ecirc;u cầu v&agrave; phản hồi xin qu&yacute; kh&aacute;ch điền v&agrave;o mẫu sau v&agrave; gửi cho ch&uacute;ng t&ocirc;i. Hoặc li&ecirc;n hệ trực tiếp qua c&aacute;c chi nh&aacute;nh.</p>
                                 </div-->
                              <form data-toggle="validator" action="/global-actions/contact-us" method="POST" accept-charset="utf-8"
                                 enctype="multipart/form-data">
                                 <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_donvi">Đơn vị</label>
                                          <input required data-error="Vui lòng kiểm tra lại đơn vị" type="text" class="form-control" id="aics_donvi" placeholder="Đơn vị (*)" autocomplete="off"
                                             name="company" value="">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_tinhthanh">Tỉnh/th&agrave;nh (*)</label>
                                          <select required class="form-control selectpicker" name="city_id" id="aics_tinhthanh">
                                             <option value="">Tỉnh/th&agrave;nh (*)</option>
                                             <option value="3922" >An Giang</option>
                                             <option value="3940" >B&agrave; Rịa</option>
                                             <option value="3962" >Bắc Giang</option>
                                             <option value="3963" >Bắc Kạn</option>
                                             <option value="3964" >Bạc Li&ecirc;u</option>
                                             <option value="3965" >Bắc Ninh</option>
                                             <option value="3923" >Bến Tre</option>
                                             <option value="3966" >B&igrave;nh Dương</option>
                                             <option value="3967" >B&igrave;nh Phước</option>
                                             <option value="3942" >B&igrave;nh Thuận</option>
                                             <option value="3941" >B&igrave;nh Định</option>
                                             <option value="3968" >C&agrave; Mau</option>
                                             <option value="3978" >Cần Thơ</option>
                                             <option value="3924" >Cao Bằng</option>
                                             <option value="3969" >Đ&agrave; Nẵng</option>
                                             <option value="3979" >Đak Lak</option>
                                             <option value="3982" >Đak N&ocirc;ng</option>
                                             <option value="3983" >Điện Bi&ecirc;n</option>
                                             <option value="3938" >Đồng Nai</option>
                                             <option value="3925" >Đồng Th&aacute;p</option>
                                             <option value="3943" >Gia Lai</option>
                                             <option value="3944" >H&agrave; Giang</option>
                                             <option value="3971" >H&agrave; Nam</option>
                                             <option value="3939" >H&agrave; Nội</option>
                                             <option value="3945" >H&agrave; Tĩnh</option>
                                             <option value="3970" >Hải Dương</option>
                                             <option value="3926" >Hải Ph&ograve;ng</option>
                                             <option value="3984" >Hậu Giang</option>
                                             <option value="3927" >Hồ Ch&iacute; Minh</option>
                                             <option value="3946" >H&ograve;a B&igrave;nh</option>
                                             <option value="3972" >Hưng Y&ecirc;n</option>
                                             <option value="3947" >Kh&aacute;nh H&ograve;a</option>
                                             <option value="3928" >Ki&ecirc;n Giang</option>
                                             <option value="3948" >Kon Tum</option>
                                             <option value="3980" >Lai Ch&acirc;u</option>
                                             <option value="3929" >L&acirc;m Đồng</option>
                                             <option value="3937" >Lạng Sơn</option>
                                             <option value="3981" >L&agrave;o Cai</option>
                                             <option value="3930" >Long An</option>
                                             <option value="3973" >Nam Định</option>
                                             <option value="3949" >Nghệ An</option>
                                             <option value="3950" >Ninh B&igrave;nh</option>
                                             <option value="3951" >Ninh Thuận</option>
                                             <option value="3974" >Ph&uacute; Thọ</option>
                                             <option value="3952" >Ph&uacute; Y&ecirc;n</option>
                                             <option value="3953" >Quảng B&igrave;nh</option>
                                             <option value="3975" >Quảng Nam</option>
                                             <option value="3954" >Quảng Ng&atilde;i</option>
                                             <option value="3931" >Quảng Ninh</option>
                                             <option value="3955" >Quảng Trị</option>
                                             <option value="3956" >S&oacute;c Trăng</option>
                                             <option value="3932" >Sơn La</option>
                                             <option value="3933" >T&acirc;y Ninh</option>
                                             <option value="3935" >Th&aacute;i B&igrave;nh</option>
                                             <option value="3976" >Th&aacute;i Nguy&ecirc;n</option>
                                             <option value="3934" >Thanh H&oacute;a</option>
                                             <option value="3957" >Thừa Thi&ecirc;n - Huế</option>
                                             <option value="3936" >Tiền Giang</option>
                                             <option value="3958" >Tr&agrave; Vinh</option>
                                             <option value="3959" >Tuy&ecirc;n Quang</option>
                                             <option value="3960" >Vĩnh Long</option>
                                             <option value="3977" >Vĩnh Ph&uacute;c</option>
                                             <option value="4063" >Vũng T&agrave;u</option>
                                             <option value="3961" >Y&ecirc;n B&aacute;i</option>
                                          </select>
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_diachi">Địa chỉ</label>
                                          <input type="text" class="form-control" id="aics_diachi" placeholder="Địa chỉ" autocomplete="off"
                                             name="address" value="">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_website">Địa chỉ website</label>
                                          <input type="text" class="form-control" id="aics_website" placeholder="Địa chỉ website" autocomplete="off"
                                             name="website" value="">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_dienthoai">Điện thoại cơ quan</label>
                                          <input type="text" class="form-control" name="phone" id="aics_dienthoai"
                                             placeholder="Điện thoại cơ quan" autocomplete="off" value="">
                                       </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_nguoilh">Người li&ecirc;n hệ</label>
                                          <input required data-error="Vui lòng kiểm tra lại người liên hệ" type="text" class="form-control" id="aics_nguoilh" placeholder="Người li&ecirc;n hệ (*)"
                                             autocomplete="off" name="name" value="">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="ntv_chucdanh">Chức danh/vị tr&iacute; c&ocirc;ng việc</label>
                                          <input type="text" class="form-control" id="ntv_chucdanh"
                                             placeholder="Chức danh/vị tr&iacute; c&ocirc;ng việc" name="work_position"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_gioitinh">Giới t&iacute;nh</label>
                                          <select class="form-control selectpicker" name="sex" id="aics_gioitinh">
                                             <option value="1" >Nam</option>
                                             <option value="0" selected>Nữ</option>
                                             <option value="2" >Kh&aacute;c</option>
                                          </select>
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_email">Email</label>
                                          <input required data-error="Vui lòng kiểm tra lại email" type="email" class="form-control" id="aics_email" placeholder="Email (*)" autocomplete="off"
                                             name="email" value="">
                                          <div class="help-block with-errors"></div>
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_dienthoaidd">Điện thoại di động</label>
                                          <input required data-error="Vui lòng kiểm tra lại điện thoại "  type="text" class="form-control" name="mobile_phone" id="aics_dienthoaidd"
                                             placeholder="Điện thoại di động (*)" autocomplete="off" value="">
                                       </div>
                                    </div>
                                    <div class="col-sm-12">
                                       <div class="form-group">
                                          <textarea required data-error="Vui lòng kiểm tra nội dung "   class="form-control" rows="7" placeholder="Nội dung (*)"
                                             name="content"></textarea>
                                       </div>
                                       <div class="form-group">
                                          <p>Đ&iacute;nh k&egrave;m t&agrave;i liệu (Tối đa 1MB): rar, zip, doc, xls, gif, jpg, png, bmp.</p>
                                          <div class="text-right">
                                             <input type="file" name="attachment" id="file-1" class="inputfile inputfile-1"
                                                data-multiple-caption="{count} files selected"/>
                                             <label class="btn btn-primary" for="file-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                                   <path
                                                      d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                                </svg>
                                                <span style="text-transform: uppercase;">Đ&iacute;nh k&egrave;m file&hellip;</span>
                                             </label>
                                          </div>
                                       </div>
                                       <div class="checkbox">
                                          <label>
                                          <input class="iCheck" type="checkbox" name="send_copy" 
                                             value="1" > Gửi một bản sao nội dung li&ecirc;n hệ n&agrave;y tới hộp thư của bạn
                                          </label>
                                       </div>
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <div id="contactBoxCaptcha"></div>
                                             </div>
                                             <div class="col-sm-6 text-right">
                                                <button type="submit" class="btn btn-primary" style="text-transform: uppercase;">Gửi ngay</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           <div class="col-sm-6">
                              <h3 class="title-udc">Đăng k&yacute; nhận tư vấn sản phẩm</h3>
                              <!--div class="text-center"><p>Để đăng k&yacute; nhận tư vấn sản phẩm xin qu&yacute; kh&aacute;ch điền v&agrave;o mẫu sau v&agrave; gửi cho ch&uacute;ng t&ocirc;i. Hoặc li&ecirc;n hệ trực tiếp qua c&aacute;c chi nh&aacute;nh.</p>
                                 </div-->
                              <form action="/global-actions/contact-us" method="POST" accept-charset="utf-8"
                                 enctype="multipart/form-data">
                                 <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                 <input type="hidden" name="type" value="1">
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_donvi">Đơn vị</label>
                                          <input required data-error="Vui lòng kiểm tra lại đơn vị" type="text" class="form-control" id="ntv_donvi" placeholder="Đơn vị (*)" name="adv_company"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_tinhthanh">Tỉnh/th&agrave;nh (*)</label>
                                          <select class="form-control selectpicker" name="adv_city_id" id="ntv_tinhthanh">
                                             <option value="">Tỉnh/th&agrave;nh (*)</option>
                                             <option value="3922" >An Giang</option>
                                             <option value="3940" >B&agrave; Rịa</option>
                                             <option value="3962" >Bắc Giang</option>
                                             <option value="3963" >Bắc Kạn</option>
                                             <option value="3964" >Bạc Li&ecirc;u</option>
                                             <option value="3965" >Bắc Ninh</option>
                                             <option value="3923" >Bến Tre</option>
                                             <option value="3966" >B&igrave;nh Dương</option>
                                             <option value="3967" >B&igrave;nh Phước</option>
                                             <option value="3942" >B&igrave;nh Thuận</option>
                                             <option value="3941" >B&igrave;nh Định</option>
                                             <option value="3968" >C&agrave; Mau</option>
                                             <option value="3978" >Cần Thơ</option>
                                             <option value="3924" >Cao Bằng</option>
                                             <option value="3969" >Đ&agrave; Nẵng</option>
                                             <option value="3979" >Đak Lak</option>
                                             <option value="3982" >Đak N&ocirc;ng</option>
                                             <option value="3983" >Điện Bi&ecirc;n</option>
                                             <option value="3938" >Đồng Nai</option>
                                             <option value="3925" >Đồng Th&aacute;p</option>
                                             <option value="3943" >Gia Lai</option>
                                             <option value="3944" >H&agrave; Giang</option>
                                             <option value="3971" >H&agrave; Nam</option>
                                             <option value="3939" >H&agrave; Nội</option>
                                             <option value="3945" >H&agrave; Tĩnh</option>
                                             <option value="3970" >Hải Dương</option>
                                             <option value="3926" >Hải Ph&ograve;ng</option>
                                             <option value="3984" >Hậu Giang</option>
                                             <option value="3927" >Hồ Ch&iacute; Minh</option>
                                             <option value="3946" >H&ograve;a B&igrave;nh</option>
                                             <option value="3972" >Hưng Y&ecirc;n</option>
                                             <option value="3947" >Kh&aacute;nh H&ograve;a</option>
                                             <option value="3928" >Ki&ecirc;n Giang</option>
                                             <option value="3948" >Kon Tum</option>
                                             <option value="3980" >Lai Ch&acirc;u</option>
                                             <option value="3929" >L&acirc;m Đồng</option>
                                             <option value="3937" >Lạng Sơn</option>
                                             <option value="3981" >L&agrave;o Cai</option>
                                             <option value="3930" >Long An</option>
                                             <option value="3973" >Nam Định</option>
                                             <option value="3949" >Nghệ An</option>
                                             <option value="3950" >Ninh B&igrave;nh</option>
                                             <option value="3951" >Ninh Thuận</option>
                                             <option value="3974" >Ph&uacute; Thọ</option>
                                             <option value="3952" >Ph&uacute; Y&ecirc;n</option>
                                             <option value="3953" >Quảng B&igrave;nh</option>
                                             <option value="3975" >Quảng Nam</option>
                                             <option value="3954" >Quảng Ng&atilde;i</option>
                                             <option value="3931" >Quảng Ninh</option>
                                             <option value="3955" >Quảng Trị</option>
                                             <option value="3956" >S&oacute;c Trăng</option>
                                             <option value="3932" >Sơn La</option>
                                             <option value="3933" >T&acirc;y Ninh</option>
                                             <option value="3935" >Th&aacute;i B&igrave;nh</option>
                                             <option value="3976" >Th&aacute;i Nguy&ecirc;n</option>
                                             <option value="3934" >Thanh H&oacute;a</option>
                                             <option value="3957" >Thừa Thi&ecirc;n - Huế</option>
                                             <option value="3936" >Tiền Giang</option>
                                             <option value="3958" >Tr&agrave; Vinh</option>
                                             <option value="3959" >Tuy&ecirc;n Quang</option>
                                             <option value="3960" >Vĩnh Long</option>
                                             <option value="3977" >Vĩnh Ph&uacute;c</option>
                                             <option value="4063" >Vũng T&agrave;u</option>
                                             <option value="3961" >Y&ecirc;n B&aacute;i</option>
                                          </select>
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_diachi">Địa chỉ</label>
                                          <input type="text" class="form-control" id="ntv_diachi" placeholder="Địa chỉ" name="adv_address"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_website">Địa chỉ website</label>
                                          <input type="text" class="form-control" id="ntv_website" placeholder="Địa chỉ website" name="adv_website"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="ntv_dienthoai">Điện thoại cơ quan</label>
                                          <input type="text" class="form-control" id="ntv_dienthoai" placeholder="Điện thoại cơ quan" name="adv_phone"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="ntv_spquantam">Sản phẩm quan t&acirc;m</label>
                                          <select class="form-control selectpicker" name="adv_interested_product" id="ntv_spquantam">
                                             <option value="0">Sản phẩm quan t&acirc;m</option>
                                             <option value="28" >Fast Business Online</option>
                                             <option value="29" >Fast Business</option>
                                             <option value="30" >Fast Financial</option>
                                             <option value="33" >Fast DMS Online</option>
                                             <option value="32" >Fast HRM Online</option>
                                             <option value="31" >Fast CRM Online</option>
                                             <option value="35" >Fast Accounting</option>
                                             <option value="34" >Fast Accounting Online</option>
                                             <option value="36" >Fast Accounting Online For Education</option>
                                          </select>
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" >Y&ecirc;u cầu giới thiệu trực tiếp</label>
                                          <select class="form-control selectpicker" name="direct_request">
                                             <option value="">Y&ecirc;u cầu giới thiệu trực tiếp</option>
                                             <option value="0" >Kh&ocirc;ng</option>
                                             <option value="1" >C&oacute;</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_nguoilh">Người li&ecirc;n hệ</label>
                                          <input required data-error="Vui lòng kiểm tra lại người liên hệ" type="text" class="form-control" id="ntv_nguoilh" placeholder="Người li&ecirc;n hệ (*)" name="adv_name"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="ntv_chucdanh">Chức danh/vị tr&iacute; c&ocirc;ng việc</label>
                                          <input type="text" class="form-control" id="ntv_chucdanh"
                                             placeholder="Chức danh/vị tr&iacute; c&ocirc;ng việc" name="adv_work_position"
                                             value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_gioitinh">Giới t&iacute;nh</label>
                                          <select class="form-control selectpicker" name="adv_sex" id="ntv_gioitinh">
                                             <option value="1" >Nam</option>
                                             <option value="0" selected>Nữ</option>
                                             <option value="2" >Kh&aacute;c</option>
                                          </select>
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_dienthoaidd">Email</label>
                                          <input required data-error="Vui lòng kiểm tra lại điện thoại "  type="text" class="form-control" name="adv_email" id="ntv_email"
                                             placeholder="Email (*)" value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_dienthoaidd">Điện thoại di động</label>
                                          <input required data-error="Vui lòng kiểm tra lại điện thoại "  type="text" class="form-control" name="adv_mobile_phone" id="ntv_dienthoaidd"
                                             placeholder="Điện thoại di động (*)" value="" autocomplete="off">
                                       </div>
                                       <div class="form-group">
                                          <label class="sr-only" for="aics_dienthoaidd">Y&ecirc;u cầu tư vấn</label>
                                          <input type="text" class="form-control" name="adv_subject" id="ntv_tuvan"
                                             placeholder="Y&ecirc;u cầu tư vấn" value="" autocomplete="off">
                                       </div>
                                    </div>
                                    <div class="col-sm-12">
                                       <div class="form-group">
                                          <textarea class="form-control" rows="4" placeholder="Nội dung"
                                             name="adv_content"></textarea>
                                       </div>
                                       <div class="checkbox">
                                          <label>
                                          <input class="iCheck" type="checkbox" name="adv_send_copy"
                                             value="1" > Gửi một bản sao nội dung li&ecirc;n hệ n&agrave;y tới hộp thư của bạn
                                          </label>
                                       </div>
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <div id="advisoryBoxCaptcha"></div>
                                             </div>
                                             <div class="col-sm-6 text-right">
                                                <button type="submit" class="btn btn-primary" style="text-transform: uppercase;">Gửi ngay</button>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <p class="bold">Những mục đ&aacute;nh dấu (*) xin ghi đầy đủ th&ocirc;ng tin</p>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection