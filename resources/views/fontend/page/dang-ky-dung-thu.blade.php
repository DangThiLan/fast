@extends('fontend.layouts.index')
@section('content')
	<main class="main">
               <div class="container">
                  <div class="container">
                     <div class="main-left">
                        <nav class="menu-left aside-left">
                              <h3 class="title-left">Fast Business Online</h3>
                              <ul>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/tinhnangchung') }}"
                                       title="T&iacute;nh năng chung">Tính năng chung</a>
                                 </li>
                                 
                                 <li>
                                    <a class="active"
                                       href="{{ url('/home/dangkydungthu') }}"
                                       title="Đăng k&yacute; d&ugrave;ng thử">Đăng k&yacute; d&ugrave;ng thử</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/banggiachung') }}"
                                       title="Bảng gi&aacute;">Bảng gi&aacute;</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/khachhang') }}"
                                       title="Kh&aacute;ch h&agrave;ng">Kh&aacute;ch h&agrave;ng</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/download') }}"
                                       title="Download t&agrave;i liệu">Download t&agrave;i liệu</a>
                                 </li>
                                 
                              </ul>
                           </nav>
                        <div class="related-products aside-left">
                              <h3 class="title-left">﻿Sản phẩm c&ugrave;ng nh&oacute;m</h3>
                              <ul>
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online"><img src="{{ url('images/DMS.jpg') }}"
                                          alt="MBSOFT DMS Online" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online">MBSOFT DMS Online</a></h4>
                                    </div>
                                 </li>
                                 
                                 
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial"><img src="{{ url('images/sp3.jpg')}}"
                                          alt="MBSOFT Financial" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial">MBSOFT Financial</a></h4>
                                    </div>
                                 </li>
                                
                              </ul>
                           </div>
                     </div>
                     <div class="main-right">
                        <div class="content-detail">
                           <p>Những mục đ&aacute;nh dấu (*) xin ghi đầy đủ th&ocirc;ng tin</p>
                           <form class="form-horizontal form-horizontal-left" action="/global-actions/trial-register/28"
                              method="POST" accept-charset="utf-8"
                              enctype="multipart/form-data">
                              <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Đơn vị (*)</label>
                                 <div class="col-sm-9">
                                    <input type="text" required class="form-control" placeholder="" name="company" value="" autocomplete="off">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Tỉnh/th&agrave;nh (*)</label>
                                 <div class="col-sm-9">
                                    <select class="form-control selectpickers" name="city_id" required>
                                       <option value="">Chọn tỉnh/th&agrave;nh</option>
                                       <option value="3922" >An Giang</option>
                                       <option value="3940" >B&agrave; Rịa</option>
                                       <option value="3962" >Bắc Giang</option>
                                       <option value="3963" >Bắc Kạn</option>
                                       <option value="3964" >Bạc Li&ecirc;u</option>
                                       <option value="3965" >Bắc Ninh</option>
                                       <option value="3923" >Bến Tre</option>
                                       <option value="3966" >B&igrave;nh Dương</option>
                                       <option value="3967" >B&igrave;nh Phước</option>
                                       <option value="3942" >B&igrave;nh Thuận</option>
                                       <option value="3941" >B&igrave;nh Định</option>
                                       <option value="3968" >C&agrave; Mau</option>
                                       <option value="3978" >Cần Thơ</option>
                                       <option value="3924" >Cao Bằng</option>
                                       <option value="3969" >Đ&agrave; Nẵng</option>
                                       <option value="3979" >Đak Lak</option>
                                       <option value="3982" >Đak N&ocirc;ng</option>
                                       <option value="3983" >Điện Bi&ecirc;n</option>
                                       <option value="3938" >Đồng Nai</option>
                                       <option value="3925" >Đồng Th&aacute;p</option>
                                       <option value="3943" >Gia Lai</option>
                                       <option value="3944" >H&agrave; Giang</option>
                                       <option value="3971" >H&agrave; Nam</option>
                                       <option value="3939" >H&agrave; Nội</option>
                                       <option value="3945" >H&agrave; Tĩnh</option>
                                       <option value="3970" >Hải Dương</option>
                                       <option value="3926" >Hải Ph&ograve;ng</option>
                                       <option value="3984" >Hậu Giang</option>
                                       <option value="3927" >Hồ Ch&iacute; Minh</option>
                                       <option value="3946" >H&ograve;a B&igrave;nh</option>
                                       <option value="3972" >Hưng Y&ecirc;n</option>
                                       <option value="3947" >Kh&aacute;nh H&ograve;a</option>
                                       <option value="3928" >Ki&ecirc;n Giang</option>
                                       <option value="3948" >Kon Tum</option>
                                       <option value="3980" >Lai Ch&acirc;u</option>
                                       <option value="3929" >L&acirc;m Đồng</option>
                                       <option value="3937" >Lạng Sơn</option>
                                       <option value="3981" >L&agrave;o Cai</option>
                                       <option value="3930" >Long An</option>
                                       <option value="3973" >Nam Định</option>
                                       <option value="3949" >Nghệ An</option>
                                       <option value="3950" >Ninh B&igrave;nh</option>
                                       <option value="3951" >Ninh Thuận</option>
                                       <option value="3974" >Ph&uacute; Thọ</option>
                                       <option value="3952" >Ph&uacute; Y&ecirc;n</option>
                                       <option value="3953" >Quảng B&igrave;nh</option>
                                       <option value="3975" >Quảng Nam</option>
                                       <option value="3954" >Quảng Ng&atilde;i</option>
                                       <option value="3931" >Quảng Ninh</option>
                                       <option value="3955" >Quảng Trị</option>
                                       <option value="3956" >S&oacute;c Trăng</option>
                                       <option value="3932" >Sơn La</option>
                                       <option value="3933" >T&acirc;y Ninh</option>
                                       <option value="3935" >Th&aacute;i B&igrave;nh</option>
                                       <option value="3976" >Th&aacute;i Nguy&ecirc;n</option>
                                       <option value="3934" >Thanh H&oacute;a</option>
                                       <option value="3957" >Thừa Thi&ecirc;n - Huế</option>
                                       <option value="3936" >Tiền Giang</option>
                                       <option value="3958" >Tr&agrave; Vinh</option>
                                       <option value="3959" >Tuy&ecirc;n Quang</option>
                                       <option value="3960" >Vĩnh Long</option>
                                       <option value="3977" >Vĩnh Ph&uacute;c</option>
                                       <option value="4063" >Vũng T&agrave;u</option>
                                       <option value="3961" >Y&ecirc;n B&aacute;i</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Địa chỉ</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="" name="address" value="" autocomplete="off">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Địa chỉ website</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Địa chỉ website" name="website" value="" autocomplete="off">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Điện thoại cơ quan</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="" name="phone" value="" autocomplete="off">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Người li&ecirc;n hệ (*)</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="" name="name" value="" autocomplete="off" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Chức danh/vị tr&iacute; c&ocirc;ng việc <span style="margin-left: -10px;position: relative;float: right;right: -10px;">(*)</span></label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="" name="work_position" value="" autocomplete="off" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Giới t&iacute;nh (*)</label>
                                 <div class="col-sm-9">
                                    <select class="form-control selectpickers" name="sex" required>
                                       <option value="1" >Nam</option>
                                       <option value="0" selected>Nữ</option>
                                       <option value="2" >Kh&aacute;c</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Email (*)</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="" name="email" value="" autocomplete="off" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Điện thoại di động (*)</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="" name="mobile_phone" value="" autocomplete="off" required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Mục đ&iacute;ch t&igrave;m hiểu (*)</label>
                                 <div class="col-sm-9">
                                    <select class="form-control selectpickers" name="goal" required>
                                       <option value="" selected>Chọn mục đ&iacute;ch t&igrave;m hiểu</option>
                                       <option value="0" >Doanh nghiệp t&igrave;m hiểu ứng dụng</option>
                                       <option value="1" >Sinh vi&ecirc;n thực tập, t&igrave;m hiểu</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Y&ecirc;u cầu giới thiệu trực tiếp </label>
                                 <div class="col-sm-9">
                                    <select class="form-control selectpickers" name="direct_request">
                                       <option value="0" selected>Kh&ocirc;ng</option>
                                       <option value="1" >C&oacute;</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-3 control-label">Nội dung</label>
                                 <div class="col-sm-9">
                                    <textarea class="form-control" rows="5" name="content"></textarea>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-12">
                                    <div class="checkbox">
                                       <label>
                                       <input class="iCheck" type="checkbox" name="send_copy"> Gửi một bản sao nội dung li&ecirc;n hệ n&agrave;y tới hộp thư của bạn
                                       </label>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-6">
                                    <div id="contactBoxCaptcha"></div>
                                 </div>
                                 <div class="col-sm-6 text-right">
                                    <button type="submit" class="btn btn-primary" style="text-transform: uppercase;">Gửi ngay</button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
            </main>
@endsection