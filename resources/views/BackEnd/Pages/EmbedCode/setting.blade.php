@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý mã nhúng - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Quản lý mã nhúng
            </a>
           
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5> Quản lý mã nhúng </h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal">
                             {{ csrf_field() }}
                            <div class="control-group">
                                <label class="control-label">Mã nhúng phần header</label>
                                <div class="controls">
                                    @php
                                        $getHD = DB::table('embed_code')->where('em_option', 1)->get();
                                    @endphp
                                    @if($getHD->count() > 0)
                                        <textarea name="ma_nhung_header" class="span11" style="min-height: 300px">{{$getHD[0]->em_code}}</textarea>
                                    @else
                                        <textarea name="ma_nhung_header" class="span11" style="min-height: 300px"></textarea>
                                    @endif
                                    
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Mã nhúng phần body</label>
                                <div class="controls">
                                    @php
                                        $getBD = DB::table('embed_code')->where('em_option', 2)->get();
                                    @endphp
                                    @if($getBD->count() > 0)
                                        <textarea name="ma_nhung_body" class="span11" style="min-height: 300px">{{$getBD[0]->em_code}}</textarea>
                                    @else
                                        <textarea name="ma_nhung_body" class="span11" style="min-height: 300px"></textarea>
                                    @endif
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Mã nhúng phần footer</label>
                                <div class="controls">
                                    @php
                                        $getFT = DB::table('embed_code')->where('em_option', 3)->get();
                                    @endphp
                                    @if($getFT->count() > 0)
                                        <textarea name="ma_nhung_footer" class="span11" style="min-height: 300px">{{$getFT[0]->em_code}}</textarea>
                                    @else
                                        <textarea name="ma_nhung_footer" class="span11" style="min-height: 300px"></textarea>
                                    @endif
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection