@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý thành viên - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="{{ url('mx-admin/quan-ly-thanh-vien') }}">
                Quản lý thành viên
            </a>
            <a href="javascript::void(0)">
                Sửa
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Sửa thành viên </h5>
                </div>
                <div class="widget-content">
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" style="max-width: 800px;">
                            {{ csrf_field() }}
                            <div class="control-group">
                                <label class="control-label">Tên:</label>
                                <div class="controls">
                                    <input type="text" class="span11" name="name" value="{{ $user_info->name }}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email:</label>
                                <div class="controls">
                                    <input type="email" class="span11" name="email" value="{{ $user_info->email }}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Mật khẩu:</label>
                                <div class="controls">
                                    <input type="password" class="span11" name="password">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"> Chức Vụ </label>
                                <div class="controls">
                                    <select class="form-control form-control-line" name="level">
                                        @if(Auth::user()->level == 0)
                                            <option @if($user_info->level == 0) selected @endif value="0">Quản trị viên</option>
                                        @endif
                                        @if(Auth::user()->level < 2)
                                            <option @if($user_info->level == 1) selected @endif value="1">Kiểm duyệt</option>
                                        @endif
                                            <option @if($user_info->level == 2) selected @endif value="2">Thành viên</option>
                                    </select>
                                </div>
                            </div>
                                <div class="controls">
                                    <button type="submit" class="btn btn-success"> Đăng </button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection