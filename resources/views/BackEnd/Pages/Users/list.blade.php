@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý thành viên - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Quản lý thành viên
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="widget-box" style="max-width: 600px;">
            <div class="widget-title">
                <span class="icon"><i class="icon-ok"></i></span>
                <h5> Thành viên </h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Chức vụ</th>
                            <th style="width: 120px">Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_user as $us)
                            <tr>
                                <td style="text-align: center;">{{ $us->name }}</td>
                                <td style="text-align: center;">
                                    @php
                                        switch ($us->level)
                                        {
                                            case 0:
                                                echo "<span class='label label-warning'> Quản trị viên </span>";
                                                break;
                                            
                                            case 1:
                                                echo "<span class='label label-info'> Kiểm duyệt </span>";
                                                break;
                                            case 2:
                                                echo "<span class='label label-success'> Thành viên </span>";
                                                break;
                                        }
                                    @endphp
                                </td>
                                <td style="text-align: center;">
                                    <a class="tip" href="{{ url('mx-admin/sua-thanh-vien/'.$us->id) }}" data-original-title="Sửa"><i class="icon-pencil"></i> Sửa </a>
                                    &nbsp;
                                    <a class="tip" href="{{ url('mx-admin/xoa-thanh-vien/'.$us->id) }}" data-original-title="Xóa"><i class="icon-remove"></i> Xóa </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection