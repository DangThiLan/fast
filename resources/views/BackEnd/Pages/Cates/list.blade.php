@extends('BackEnd.LayOut.master')
@section('title', 'Danh mục sản phẩm - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                 Danh mục sản phẩm
            </a>
        </div>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h5> Danh mục sản phẩm </h5>
                </div>
                <div class="widget-content">
                    <div class="todo">
                        <ul>
                            @php
                                showCategories($cates);
                            @endphp
                            {{-- @foreach($cates as $ct)
                                <li class="clearfix">
                                    <div class="txt"> {{ $ct->cat_name }} </div>
                                    <div class="pull-right"> 
                                        <a class="tip" href="{{ url('mx-admin/sua-danh-muc/'.$ct->cat_id) }}" data-original-title="Sửa"><i class="icon-pencil"></i> Sửa </a>
                                        &nbsp;
                                        <a class="tip" href="{{ url('mx-admin/xoa-danh-muc/'.$ct->cat_id) }}" data-original-title="Xóa"><i class="icon-remove"></i> Xóa </a>
                                    </div>
                                </li>
                            @endforeach --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection