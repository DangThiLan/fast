@extends('BackEnd.LayOut.master')
@section('title', 'Quản lý video - Hệ thống quản trị website')
@section('main-content')

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="{{ url('mx-admin') }}" title="Trang chủ" class="tip-bottom">
                <i class="icon-home"></i> Trang chủ
            </a>
            <a href="">
                Quản lý video 
            </a>
        </div>
        <h1 class="ttl-add-use" style="font-size: 25px"> Danh sách Video </h1>
    </div>
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">

        @include('Notify.note')

        <div class="row-fluid">
            <div class="">
                <style type="text/css">
                    .item-video {
                        padding: 10px;
                        border: 1px solid #ddd;
                        margin-bottom: 20px;
                        background: #fff;
                    }
                    .item-video iframe { width: 100%!important } 
                </style>
                @if(count($video) > 0 )
                    @foreach($video as $videoItem)
                        <div class="span3 item-video">
                            <form class="form-horizontal form-material" action="{{ url('mx-admin/update-video/'.$videoItem->vd_id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="iframe-video">
                                    {!! $videoItem->vd_src !!}
                                </div>
                                <div class="text-iframe">
                                    <textarea name="ma_nhung_youtube" style="width: 94.8%; min-height: 115px;">{{ $videoItem->vd_src }}</textarea>
                                </div>
                                <div class="btn-update">
                                    <button class="btn btn-sm btn-warning pull-left"> Cập Nhật </button>
                                    <a class="btn btn-sm btn-danger pull-right" href="{{ url('mx-admin/video/xoa/'.$videoItem->vd_id) }}"> Xóa </a>
                                </div>
                            </form>
                            <div style="clear: both;"></div>
                        </div>
                    @endforeach
                @else
                    <h2 style="color: red">Bạn chưa có video nào</h2>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection