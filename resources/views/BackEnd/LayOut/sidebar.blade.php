<div id="sidebar">
    <a href="#" class="visible-phone">
        <i class="icon icon-home"></i> Danh mục quản trị
    </a>
    <ul>
        {{-- done --}}
        <li>
            <a href="{{ url('mx-admin/cau-hinh-chung') }}">
                <i class="icon icon-signal"></i>
                <span> Cấu hình chung </span>
            </a>
        </li>

        {{-- done --}}
        <li class="submenu">
            <a href="{{ url('mx-admin/quan-ly-thanh-vien') }}">
                <i class="fa fa-users" aria-hidden="true"></i>
                <span> Quản lý thành viên </span>
                <span class="icon-chevron-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/quan-ly-thanh-vien') }}">Tất cả</a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/them-thanh-vien') }}">Thêm mới</a>
                </li>
            </ul>
        </li>
    
        {{-- done --}}
        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="icon icon-th-list"></i>
                <span> Quản lý danh mục </span>
                <span class="icon-chevron-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/danh-muc') }}">Tất cả</a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/them-danh-muc') }}">Thêm mới</a>
                </li>
            </ul>
        </li>
        
        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                <span> Tin tức </span>
                <span class="icon-chevron-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/quan-ly-bai-viet') }}">Tất cả</a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/viet-bai-moi') }}">Thêm mới</a>
                </li>
            </ul>
        </li>
        
       
        <li class="submenu">
            <a href="javascript::void(0)">
                <i class="fa fa-product-hunt" aria-hidden="true"></i>
                <span> Sản Phẩm </span>
                <span class="icon-chevron-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/san-pham') }}">Tất cả</a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/san-pham/them-moi') }}">Thêm mới</a>
                </li>
            </ul>
        </li>

       
        
        {{-- done --}}
        <li>
            <a href="{{ url('mx-admin/logo') }}">
                <i class="fa fa-audio-description" aria-hidden="true"></i>
                <span>Logo</span>
            </a>
        </li>
        
        {{-- done --}}
        <li>
            <a href="{{ url('mx-admin/slider') }}">
                <i class="icon icon-tint"></i>
                <span>Slider</span>
            </a>
        </li>
        
        <li>
            <a href="{{ url('mx-admin/trang-hoi-dong-khoa-hoc') }}">
                <i class="fa fa-inr" aria-hidden="true"></i>
                <span>Trang hội đồng khoa hoc</span>
            </a>
        </li>

        <li>
            <a href="{{ url('mx-admin/trang-gioi-thieu') }}">
                <i class="fa fa-inr" aria-hidden="true"></i>
                <span>Trang giới thiệu</span>
            </a>
        </li>
        
        {{-- done --}}
        <li>
            <a href="{{ url('mx-admin/quan-ly-ma-nhung') }}">
                <i class="fa fa-code" aria-hidden="true"></i>
                <span>Quản lý mã nhúng</span>
            </a>
        </li>
        
        {{-- done --}}
        <li class="submenu">
            <a href="#">
                <i class="fa fa-video-camera" aria-hidden="true"></i>
                <span>Video</span>
                <span class="icon-chevron-down" style="font-size: 12px;"></span>
            </a>
            <ul>
                <li>
                    <a href="{{ url('mx-admin/video') }}"> Tất cả </a>
                </li>
                <li>
                    <a href="{{ url('mx-admin/video/dang-moi') }}"> Đăng mới </a>
                </li>
            </ul>
        </li>

        
        <li>
            <a href="{{ url('mx-admin/mo-rong') }}">
                <i class="icon icon-th"></i>
                <span>Phần mở rộng</span>
            </a>
        </li>

        
        
        

    </ul>
</div>