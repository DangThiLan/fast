<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li class="">
            <a href="javascript::void(0)">
                <span class="text" style="color: #eee">Xin chào  <span style="color: #fff">{{ Auth::user()->name }}</span> </span>
            </a>
        </li>
        <li class="">
            <a href="{{ url('/') }}">
                <span class="text" style="color: #eee"> Xem website </span>
            </a>
        </li>
        <li class="">
            <a href="{{ url('mx-admin/logout') }}">
                <i class="icon icon-share-alt" style="color: #eee"></i>
                <span class="text" style="color: #eee"> Đăng xuất </span>
            </a>
        </li>
    </ul>
</div>