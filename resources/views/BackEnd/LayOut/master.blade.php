<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@yield('title')</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        @include('BackEnd.Vendor.inc_css')
        </head>
    <body>
        
        <div id="header">
            <h1>
                <a href="{{ url( env('URL_ADMIN', '/') ) }}">
                    <img src="{{ asset('backend/img/logo-white.png') }}">
                    {{-- <span class="text-title-header">Hệ thống quản trị</span> --}}
                </a>
            </h1>
        </div>
       
        
        @include('BackEnd.LayOut.nav_top')
        
        @include('BackEnd.LayOut.sidebar')
        <!--main-container-part-->

        @yield('main-content') 
                        
        @include('BackEnd.Vendor.inc_js')           
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"> 2018 &copy; <a href="maxgo.vn">Maxgo.vn</a>
            </div>
        </div>

        <!--end-Footer-part-->
        
        <script type="text/javascript">
            // This function is called from the pop-up menus to transfer to
            // a different page. Ignore if the value returned is a null string:
            function goPage (newURL) {

                // if url is empty, skip the menu dividers and reset the menu selection to default
                if (newURL != "") {
                  
                    // if url is "-", it is this page -- reset the menu:
                    if (newURL == "-" ) {
                        resetMenu();            
                    } 
                    // else, send page to designated URL            
                    else {  
                        document.location.href = newURL;
                    }
                }
            }
            // resets the menu selection upon entry to this page:
            function resetMenu() {
               document.gomenu.selector.selectedIndex = 2;
            }
        </script>
        @yield('script')
    </body>
</html>