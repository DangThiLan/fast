<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('backend/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/bootstrap-responsive.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/fullcalendar.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/matrix-style.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/matrix-media.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/font-awesome/css/font-awesome.css') }}"  />
<link rel="stylesheet" href="{{ asset('backend/css/font-awesome.min.css') }}"  />
<link rel="stylesheet" href="{{ asset('backend/css/maxgo-admin.css') }}"  />
<link rel="stylesheet" href="{{ asset('backend/css/checkbox.css') }}"  />