@if(Session::has('success'))
    <span style="color: #05d500; margin-top: 20px; display: block;">{{ Session::get('success') }}</span>
@endif
@if(Session::has('error'))
    <span style="color: #f33; margin-top: 20px; display: block;">{{ Session::get('error') }}</span>
@endif
