<?php
@include('route_admin.php');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');


Route::get('/gioithieu', 'GioiThieuController@index');
Route::get('/cacgiaithuong', 'CacgiaithuongController@index');
Route::get('/tinhnangchung', 'TinhnangchungController@index');
Route::get('/giaiphapchuyennghanh', 'GiaiphapController@index');
Route::get('/dangkydungthu', 'DangkydungthuController@index');
Route::get('/banggiachung', 'BanggiachungController@index');
Route::get('/khachhang', 'KhachhangController@index');
Route::get('/download', 'DownloadController@index');
Route::get('/tuvanungdung', 'TuvanungdungController@index');
Route::get('/tuvanhotro', 'TuvanhotroController@index');
Route::get('/tuvancntt', 'TuvancnttController@index');
Route::get('/banggiasp1', 'Banggiasp1Controller@index');
Route::get('/banggiasp2', 'Banggiasp2Controller@index');
Route::get('/tuyendung', 'TuyendungController@index');
Route::get('/lienhe', 'LienheController@index');
Route::get('/tuvanungdung/chitiet', 'TintucController@index');
Route::get('/tintuc', 'TintucController@index');