<?php

/*
|--------------------------------------------------------------------------
| Admin
|--------------------------------------------------------------------------
|
| 
|
*/

$routerAdmin = env('URL_ADMIN', '404');

Route::get($routerAdmin, function(){
	if (Auth::check())
	{
		return redirect('mx-admin');
	}
	else
	{
		return redirect('mx-login');
	}
});

Route::group([ 'prefix' => 'mx-login' , 'namespace' => 'BackEnd', 'middleware' => 'CheckLogin' ], function() {
   	Route::get('/', 'LoginController@login');
	Route::post('/', 'LoginController@postlogin');
});

Route::group([ 'prefix' => 'mx-admin', 'namespace' => 'BackEnd', 'middleware' => 'CheckLogout'], function()
{
	Route::get('/logout', 'LoginController@logout');
	Route::get('/', 'PageController@dashboard');

	/**  Cấu hình website  **/
	Route::get('/cau-hinh-chung', 'PageController@settingWebsite');
	Route::post('/cau-hinh-chung', 'PageController@postSettingWebsite');

	/**  Danh mục menu  **/
	Route::get('/danh-muc', 'CategoryController@category');

	/**  Tạo mới danh mục  **/
	Route::get('/them-danh-muc', 'CategoryController@addCategory');
	Route::post('/them-danh-muc', 'CategoryController@postAddCategory');

	/**  Sửa danh mục  **/
	Route::get('/sua-danh-muc/{cate_id}', 'CategoryController@editCategory');
	Route::post('/sua-danh-muc/{cate_id}', 'CategoryController@postEditCategory');

	/**  Xóa danh mục  **/
	Route::get('/xoa-danh-muc/{cate_id}', 'CategoryController@deleteCategory');

	/**  Danh sách thành viên  **/
	Route::get('/quan-ly-thanh-vien', 'PageController@listUsers');

	/**  Thêm thành viên  **/
	Route::get('/them-thanh-vien', 'UserController@addUser');
	Route::post('/them-thanh-vien', 'UserController@postAddUser');

	/**  Sửa thành viên  **/
	Route::get('/sua-thanh-vien/{user_id}', 'UserController@editUser');
	Route::post('/sua-thanh-vien/{user_id}', 'UserController@postEditUser');

	/**  Xóa thành viên  **/
	Route::get('/xoa-thanh-vien/{user_id}', 'UserController@deleteUser');

	/**  Quản lý banner  **/
	Route::get('/logo', 'PageController@logo');
	Route::post('/logo', 'PageController@postLogo');

	/**  Quản lý mã nhúng  **/
	Route::get('/quan-ly-ma-nhung', 'EmbedController@settingEmCode');
	Route::post('/quan-ly-ma-nhung', 'EmbedController@postEmbedCode');

	/** Đăng sản phẩm mới **/
	Route::get('/san-pham/them-moi', 'ProductsController@addProduct');
	Route::post('/san-pham/them-moi', 'ProductsController@postAddProduct');

    Route::get('/san-pham', 'ProductsController@product');

    Route::get('/sua-san-pham/{pro_id}', 'ProductsController@editProduct');
	Route::post('/sua-san-pham/{pro_id}', 'ProductsController@postEditProduct');

	Route::get('/xoa-san-pham/{pro_id}', 'ProductsController@deleteProduct');

	/**  Cài đặt banner  **/
	Route::get('/slider', 'PageController@banner');
	Route::post('/slider', 'PageController@setBanner');

	/**  Quản lý video  **/
	Route::get('/video/dang-moi', 'PageController@video');
	Route::post('/video/dang-moi', 'PageController@postVideo');
	Route::get('/video', 'PageController@listVideo');
	Route::post('/update-video/{id_video}', 'PageController@updateVideo');
	Route::get('/video/xoa/{id_video}', 'PageController@deleteVideo');

	/** Trang liên hệ **/
	Route::get('mo-rong', 'PageController@widgetSetting');
	Route::post('mo-rong', 'PageController@pWidgetSetting');

	Route::get('trang-gioi-thieu', 'PageController@renderAbout');
	Route::post('trang-gioi-thieu', 'PageController@postRenderAbout');

	Route::get('trang-hoi-dong-khoa-hoc', 'PageController@renderKhoahoc');
	Route::post('trang-hoi-dong-khoa-hoc', 'PageController@postRenderKhoahoc');
	
	/**  Viết bài mới  **/
	Route::get('/viet-bai-moi', 'NewsController@creatNews');
	Route::post('/viet-bai-moi', 'NewsController@postCreatNews');

	Route::get('/quan-ly-bai-viet', 'NewsController@listNews');
	Route::get('/xem-bai-viet/{news_id}', 'NewsController@viewNews');
	Route::get('/sua-bai-viet/{news_id}', 'NewsController@editNews');
	Route::post('/sua-bai-viet/{news_id}', 'NewsController@postEditNews');
	Route::get('/xoa-bai-viet/{news_id}', 'NewsController@deleteNews');

	
});